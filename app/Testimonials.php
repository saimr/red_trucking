<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Testimonials extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        
        'name',
        'image',
        'details',
        'status',
        'is_deleted',
    ];

       protected $table = 'testimonials';

       


    public static function update_Or_insert($postData,$id=0){
      
      if($id){
         $postData['updated_at']  =  date("Y-m-d H:i:s") ;
         $res  = DB::table('testimonials')->where('id',$id)->update($postData);  
         $result = $res ? $id : 0 ; 
      } else {
         $postData['created_at']  =  date("Y-m-d H:i:s") ;
         DB::table('testimonials')->insert($postData); 
        $result =  DB::getPdo()->lastInsertId();        
      }
      
       return $result;
    }

    public static function getAllTestimonialsData($page=10) {

       $result = DB::table('testimonials')->
                where('is_deleted',0)->
                 orderBy('id','DESC')->
                paginate($page);

       return $result;
    }

    public static function is_exists($id){

       $result = DB::table('testimonials')->
                    select('testimonials.id')->
                    where('testimonials.id',$id)->
                    where('testimonials.is_deleted',0)->
                    first();

       return !empty($result) ? 1  : 0 ;
    }

   


    public static function get_data_by_id($id){

       $result = DB::table('testimonials')->
                    select('testimonials.*')->
                    where('testimonials.id',$id)->
                    where('testimonials.is_deleted',0)->
                    first();

       return $result;
    }

   

}
