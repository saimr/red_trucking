<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name',
        'slug',
    ];

       protected $table = 'roles';
}
