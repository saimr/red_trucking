<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
     protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name',
        'slug',
    ];

       protected $table = 'permissions';
}
