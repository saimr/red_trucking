<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Banner extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
      
        'title',
        'details',
        'image',
        'staus',
        'is_deleted',
        'updated_at',
        'deleted_at',
    ];

       protected $table = 'banners';

    public static function update_Or_insert($postData,$id=0){
      
      if($id){
         $postData['updated_at']  =  date("Y-m-d H:i:s") ;
         $result = DB::table('banners')->where('id',$id)->update($postData);  
      } else {
        $postData['created_at']  =  date("Y-m-d H:i:s") ;
        $result = DB::table('banners')->insert($postData);         
      }
      
       return $result;
    }

   

    public static function get_banner_data($type = 1){

       $result = DB::table('banners')->
                    select('banners.*')->
                    where('banners.type',$type)->
                    where('banners.status',1)->
                    where('banners.is_deleted',0)->get();

       return $result;
    }

}
