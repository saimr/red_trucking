<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;
use App\Banner;
use App\Form;
class DriverController   extends Controller
{
    //

    public function index(Request $request)
    {   

      $page_title  = config('constants.website_name').' | Truck Driver Jobs In Newark, NJ.' ;
      $description  = 'Joining the troop of RED Truck Driver comes with lots of perks. Regional, Dedicated & Always-on-road are the driving options that are available at '.config('constants.website_name').'.' ;
      $sr = 1 ;
      $driver_data = MetaData::get_data_by_meta_type(config('constants.meta_type.red_driver_page'));
      $banner_data  = Banner::get_banner_data(config('constants.banner_type.red_driver_banner'));
      $count =  (count($banner_data)-1);
      $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name').' - Driver',
                  'url'=>'driver',
                  'description'=>$description,
                  'image'=>isset($banner_data[$count]->image) && file_exists($banner_data[$count]->image) ? $banner_data[$count]->image:'' ,
                  ];
      
      return view('frontend.driver.index',compact('driver_data','banner_data','sr','page_title','description','og_meta'));

    }

    public function save(Request $request)
    {
      $post = $request ;
      $flush_data =  array('key'=>'','msg'=>'') ;

      $validation = [
                    'first_name' => 'required',
                    //'last_name' => 'required',
                    'phone' => 'required',
                    'email' => 'required|email',
                    'is_cdl' => 'required',
                    'is_twice' => 'required',
                    'is_driving_experience' => 'required',
                ] ;
      if(!empty($request->file('upload_file'))){
           $validation['upload_file'] =  'required|mimes:jpeg,png,jpg,pdf,doc,docx' ;
         }
       $customMessages = [
                            'is_cdl.required' => 'Do you hold a valid CDL Class A License?',
                            'is_twice.required' => 'Do you hold a valid TWIC card?',
                            'is_driving_experience.required' => 'Do you have 2 or more years of Tractor Trailer driving experience?',
                        ];
      $request->validate($validation,$customMessages);

      $postData = [
                  'first_name'=>isset($post['first_name'])? $post['first_name'] : '',
                  'phone'=>isset($post['phone'])? $post['phone'] : '',
                  'email'=>isset($post['email'])? $post['email'] : '',
                  'is_cdl'=>isset($post['is_cdl'])? $post['is_cdl'] : '',
                  'is_twice'=>isset($post['is_twice'])? $post['is_twice'] : '',
                  'is_driving_experience'=>isset($post['is_driving_experience'])? $post['is_driving_experience'] : '',
                  'is_inmoto_or_drayage'=>isset($post['is_inmoto_or_drayage'])? $post['is_inmoto_or_drayage'] : '',
                  'form_type'=>config('constants.form_type.career'),
                  ] ;

          $upload_file = $request->file('upload_file'); 
          if(isset($upload_file) && !empty($upload_file)) {
              $fileName = time().'.'.$upload_file->getClientOriginalExtension();  
              $upload_file->move(public_path('uploads/career'), $fileName);
              $uploded_file_name = 'uploads/career/'.$fileName;
              $postData['upload_file'] = $uploded_file_name ; 
          }

          $res = Form::update_Or_insert($postData);

            if($res){
              $flush_data['key'] = 'success' ;
              $flush_data['msg'] = 'Thank you for contacting us directly. ' ;
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong  please try again !' ;
            }

 
      return $res ? redirect('thankyou')->with($flush_data['key'], $flush_data['msg'])  : redirect()->back()->with($flush_data['key'], $flush_data['msg']);

    }
   
    
}
