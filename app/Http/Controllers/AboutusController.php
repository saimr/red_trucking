<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Page;
use App\PageGallery;
use App\Banner;
use App\MetaData;
use App\CompanyMembers;
class AboutusController   extends Controller
{
    //

    public function index(Request $request)
    {   
    
    $page_title     = 'About '.config('constants.website_name') ;
    $description     = 'Founded in 2004, RED Trucking provides comprehensive logistics, shipping and warehousing services to a wide range of domestic and international clients.' ;
  	$abouts_us_data  = Page::get_data_by_page_type(config('constants.about_us_page.about_us'));
  	$left_about_us_image  = PageGallery::get_data_by_page_id(isset($abouts_us_data->id) ? $abouts_us_data->id : 0 ,config('constants.page_gallery_type.image'));
    $right_about_us_image = PageGallery::get_right_images_by_page_id(isset($abouts_us_data->id) ? $abouts_us_data->id : 0 ,config('constants.page_gallery_type.image'));

   
    $about_us_owner_data = MetaData::get_data_by_meta_type(config('constants.meta_type.about_us_owner_data'));

    $red_family_data = MetaData::get_data_by_meta_type(config('constants.meta_type.about_us_red_family_content'));

    $safety_data = MetaData::get_data_by_meta_type(config('constants.meta_type.safety_about_us_content'));
    $sustainability_data = MetaData::get_data_by_meta_type(config('constants.meta_type.sustainability_about_us_content'));

    $all_members = CompanyMembers::getAllMemberData();

    $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name').' - About',
                  'url'=>'about',
                  'description'=>$description,
                  'image'=>isset($left_about_us_image->name) && file_exists($left_about_us_image->name) ? $left_about_us_image->name:'' ,
                  ];
    
      return view('frontend.aboutus.about_us',compact('page_title','description','abouts_us_data','left_about_us_image','right_about_us_image','safety_data','sustainability_data','about_us_owner_data','red_family_data','all_members','og_meta'));

    }

    public function safety(Request $request)
    {   
    
    $page_title     = 'We put safety first for everything.' ;
    $description     = 'Ensuring the safety of everything, from our truck drivers, delivery personnel, motoring traffic & the cargo we carry comes under our safety net.' ;

    $safety_data = MetaData::get_data_by_meta_type(config('constants.meta_type.safety_page'));

    $safety_meta = isset($safety_data->meta_data) && !empty($safety_data->meta_data) ? json_decode($safety_data->meta_data) : array() ;
    $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name').' - Safety',
                  'url'=>'about/safety',
                  'description'=>$description,
                  'image'=>isset($safety_meta->left_image) && file_exists($safety_meta->left_image) ? $safety_meta->left_image :'' ,
                  ];

    
      return view('frontend.aboutus.safety',compact('safety_data','page_title','description','og_meta'));

    }

    public function sustainability(Request $request)
    {   
    
    $page_title     = 'Ensuring least impact on environmental, economic & social.' ;
    $description     = 'We ensure our activities leave the least impact on the environment and economy. We train our drivers best to maintain the best standards.' ;

    $sustainability_data = MetaData::get_data_by_meta_type(config('constants.meta_type.sustainability_page'));

    $sustainability_meta = isset($sustainability_data->meta_data) && !empty($sustainability_data->meta_data) ? json_decode($sustainability_data->meta_data) : array() ;

    $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name').' - Sustainability',
                  'url'=>'about/sustainability',
                  'description'=>$description,
                  'image'=>isset($sustainability_meta->second_image) && file_exists($sustainability_meta->second_image) ? $sustainability_meta->second_image :'' ,
                  ];
    
      return view('frontend.aboutus.sustainability',compact('sustainability_data','page_title','description','og_meta'));

    }
    
}
