<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;
use App\Rules\ContactCapture;
use App\Form;

use App\Traits\EmailsTrait;

class ContactController   extends Controller
{
    //
  use EmailsTrait;

    public static function index(Request $request)
    {   

     $page_title     = 'Contact Us' ;  
     $description     = 'Don’t hesitate, connect with '.config('constants.website_name').' any time for quick trucking solutions.' ;  
     $transport_type = config('constants.transport_type') ; 

      $contact_us_data = MetaData::get_data_by_meta_type(config('constants.meta_type.contact_us_page_details'));

      $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name').' - Contact Us',
                  'url'=>'contact',
                  'description'=>$description,
                  'image'=> asset('images/frontend/contact/contact.png') ,
                  ];
      

      return view('frontend.contact_us.contact_us',compact('contact_us_data','transport_type','page_title','description','og_meta'));

    }
    public static function save(Request $request)
    { 

      $post = $request ;
       $flush_data =  array('key'=>'','msg'=>'') ;

      $validation = [
                    'first_name' => 'required',
                    //'last_name' => 'required',
                    'phone' => 'required',
                    'email' => 'required|email',
                    'transport_type.*' => 'required',
                    //'best_time_hour' => 'required',
                    //'best_time_minute' => 'required',
                    //'best_time' => 'required',
                    'capture'=> ['required',new ContactCapture($post['first_number'],$post['second_number'])],
                ] ;
        if($post['transport_type'] == 2 ) {
          $validation['company_name'] = 'required';
          $validation['commodity'] = 'required';
        }

      if(!isset($request->transport_type)){
         $validation['transport_type[]'] = 'required';
       }
      $customMessages = [
                          'transport_type.required' => 'The Transport field is required.',
                        ];
    $request->validate($validation,$customMessages);
    $best_time  = strtotime($post['best_time_hour'].':'.$post['best_time_minute'].' '.$post['best_time']) ;

    $postData =  array(
                    'first_name'=>isset($post['first_name'])? $post['first_name'] : '',
                    'last_name'=>isset($post['last_name'])? $post['last_name'] : '',
                    'phone'=>isset($post['phone'])? $post['phone'] : '',
                    'email'=>isset($post['email'])? $post['email'] : '',
                    'message'=>isset($post['message'])? $post['message'] : '',
                    'form_type'=>config('constants.form_type.contact'),
                    'best_time'=>$best_time,
                    'transport_type'=> $post['transport_type'] ,
                    'company_name'=>isset($post['company_name'])? $post['company_name'] : '',
                    'commodity'=>isset($post['commodity'])? $post['commodity'] : '',
                    'number_of_loads'=>isset($post['number_of_loads'])? $post['number_of_loads'] : '',
                    'cargo_weight'=>isset($post['cargo_weight'])? $post['cargo_weight'] : '',
                    'pickup_address'=>isset($post['pickup_address'])? $post['pickup_address'] : '',
                    'pickup_city'=>isset($post['pickup_city'])? $post['pickup_city'] : '',
                    'pickup_state'=>isset($post['pickup_state'])? $post['pickup_state'] : '',
                    'pickup_zip_code'=>isset($post['pickup_zip_code'])? $post['pickup_zip_code'] : '',
                    'pickup_intermodal_equipment'=>isset($post['pickup_intermodal_equipment'])? $post['pickup_intermodal_equipment'] : '',
                    'destination_address'=>isset($post['destination_address'])? $post['destination_address'] : '',
                    'destination_city'=>isset($post['destination_city'])? $post['destination_city'] : '',
                    'destination_state'=>isset($post['destination_state'])? $post['destination_state'] : '',
                    'destination_zip_code'=>isset($post['destination_zip_code'])? $post['destination_zip_code'] : '',
                    'destination_intermodal_equipment'=>isset($post['destination_intermodal_equipment'])? $post['destination_intermodal_equipment'] : '',
                 );
     
    
    $res = Form::update_Or_insert($postData);

            if($res){
                EmailsTrait::contact_us_email($res) ;
              $flush_data['key'] = 'success' ;
              $flush_data['msg'] = 'Thank you for contacting us directly. ' ;
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong  please try again !' ;
            } 

    return $res ? redirect('thankyou')->with($flush_data['key'], $flush_data['msg'])  : redirect()->back()->with($flush_data['key'], $flush_data['msg']);

       
    }
    
}
