<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;
use App\Banner;
use App\Form;
class ServiceController   extends Controller
{
    //

    public function transport(Request $request)
    {   

      $page_title  = 'Most trusted truck transport and logistics services.' ;
      $description  = 'At '.config('constants.website_name').', get the best ruck transport and logistics services. We provide expert end-to-end shipment management covering 48 states out of 50.' ;
      $transport_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_transport_page'));

       $meta_data = isset($transport_data->meta_data) && !empty($transport_data->meta_data) ? json_decode($transport_data->meta_data) : array() ;
      
      $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name').' - Transport',
                  'url'=>'service/transport',
                  'description'=>$description,
                  'image'=>isset($meta_data->section_three_image) && file_exists($meta_data->section_three_image) ? $meta_data->section_three_image:'' ,
                  ];

      return view('frontend.service.transport',compact('transport_data','page_title','description','og_meta'));

    }

     public function warehousing(Request $request)
    {   

      $page_title  = 'Tailored warehouse management solution according to ones’ needs.' ;
      $description  = 'We offer a warehouse management system that works on real-time inventory tracking, onsite digital CCTV Security, GPS Tracking on dedicated trucks, and much more.' ;
      $warehousing_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_warehousing_page'));

      $meta_data = isset($warehousing_data->meta_data) && !empty($warehousing_data->meta_data) ? json_decode($warehousing_data->meta_data) : array() ;

      $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name').' - Warehousing',
                  'url'=>'service/warehousing',
                  'description'=>$description,
                  'image'=>isset($meta_data->section_three_image) && file_exists($meta_data->section_three_image) ? $meta_data->section_three_image:'' ,
                  ];
      
      return view('frontend.service.warehousing',compact('warehousing_data','page_title','description','og_meta'));

    }

     public function logistics(Request $request)
    {   

      $page_title  = 'Logistics Warehouse Management Services.' ;
      $description  = 'With our logistics warehouse management services get your shipment done on time. We provide palletizing, real-time GPS tracking, RED chassis, crossdock, etc.' ;
      $logistics_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_logistics_page'));

       $meta_data = isset($logistics_data->meta_data) && !empty($logistics_data->meta_data) ? json_decode($logistics_data->meta_data) : array() ;

      $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name').' - Logistics',
                  'url'=>'service/logistics',
                  'description'=>$description,
                  'image'=>isset($meta_data->section_three_image) && file_exists($meta_data->section_three_image) ? $meta_data->section_three_image:'' ,
                  ];
      
      return view('frontend.service.logistics',compact('logistics_data','page_title','description','og_meta'));

    }

     public function transloading(Request $request)
    {   

      $page_title  = 'Transloading With '.config('constants.website_name').'.' ;
      $description  = 'Providing comprehensive transloading services to traverse the geographical barriers with ease.' ;
      $transloading_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_transloading_page'));

      $meta_data = isset($transloading_data->meta_data) && !empty($transloading_data->meta_data) ? json_decode($transloading_data->meta_data) : array() ;
      $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name').' - Transloading',
                  'url'=>'service/transloading',
                  'description'=>$description,
                  'image'=>isset($meta_data->section_third_image) && file_exists($meta_data->section_third_image) ? $meta_data->section_third_image:'' ,
                  ];
      
      return view('frontend.service.transloading',compact('transloading_data','page_title','description','og_meta'));

    }

    
   
    
}
