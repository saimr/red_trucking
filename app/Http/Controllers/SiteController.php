<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Page;
use App\PageGallery;
use App\Banner;
use App\Blog;
use App\MetaData;
use App\HappyClients;
use App\Testimonials;
class SiteController   extends Controller
{
    //

    public function index(Request $request)
    {   
     $page_title     = config('constants.website_name').' | Truck Transportation And Logistics Services. ' ;
     $description     = 'Providing professional truck transportation and logistics services. Based near to the top ports providing timely & reliable hauling and warehousing solutions.' ;
  	$abouts_us_data  = Page::get_data_by_page_type(config('constants.about_us_page.about_us'));
  	$about_us_image  = PageGallery::get_data_by_page_id(isset($abouts_us_data->id) ? $abouts_us_data->id : 0 ,config('constants.page_gallery_type.image'));
    $partner_data     = Page::get_data_by_page_type(config('constants.partner_page'));
    $banner_data      = Banner::get_banner_data();
     $blog_data       = Blog::getAllBlogData(3);


     $about_us_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.about_us_home_content'));

      $trust_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.trust_home_content'));
      $security_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.security_home_content'));
      $sustainability_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.sustainability_home_content'));
      $service_areas_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_areas_home_content'));
      $red_service_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.red_service_home_content'));
      $partner_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.partner_home_content'));
      $resource_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.resource_home_content'));

      $all_clients = HappyClients::getRandomData();
      $all_testimonials = Testimonials::getAllTestimonialsData();


      $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name'),
                  'url'=>'/',
                  'description'=>$description,
                  'image'=>asset('images/frontend/site-logo.png'),
                  ];

     
      return view('frontend.home.index',compact('page_title','description','abouts_us_data','about_us_image','banner_data','partner_data','blog_data','about_us_meta_data','trust_meta_data','security_meta_data','sustainability_meta_data','service_areas_meta_data','red_service_meta_data','partner_meta_data','resource_meta_data','all_clients','all_testimonials','og_meta'));

    }

    public function thank_you()
    {   

      $page_title     = 'Thank You' ;

      if (!Session::get('success'))
      {
        return redirect('/');
      }
      $message        = Session::get('success') ;

      return view('frontend.home.thank_you',compact('page_title','message'));


   }


    
}
