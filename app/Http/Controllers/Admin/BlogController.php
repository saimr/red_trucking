<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller
{
    //

     public function add(Request $request)
    {   

        return view('admin.blog.add');
    }

     public function edit($id)
    {   
       if(!Blog::is_exists($id)){
       	 return redirect()->back()->with('error', 'Blog does not exist');
       }
       $data = Blog::get_data_by_id($id);

        return view('admin.blog.add',compact('data'));
    }

    public function index(Request $request)
    {   

      $blog_data = Blog::getAllBlogData();
      return view('admin.blog.index',compact('blog_data'));
    }

    public function update(Request $request)
    {   

      $flush_data =  array('key'=>'','msg'=>'') ;
    	$post   = $request->all();
    	$validation = [
                    'title' => 'required',
                    'sort_description' => 'required|max:100',
                    'description' => 'required',
                ] ;

        $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
      
        if(!$id || !empty($request->file('images'))){
           $validation['images'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=1140,min_height=464' ;
         } 
         $customMessages = [
                            'images.dimensions' => 'Select :attribute  must be at least min 1140x464 pixels.',
                        ];
          
    	$request->validate($validation,$customMessages);

    	$postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'sort_description'=>isset($post['sort_description'])? $post['sort_description'] : '',
                    'description'=>isset($post['description'])? $post['description'] : '',
           			 );

      if(Blog::check_unique($post['title'],$id)){
         return redirect()->back()->with('error', 'Blog already exists');
       }

    	$images = $request->file('images'); 
          if(isset($images) && !empty($images) ) {
          	  $fileName = time().'.'.$images->getClientOriginalExtension();  
              $images->move('uploads/blogs', $fileName);
              $uploded_file_name = 'uploads/blogs/'.$fileName;
              $postData['image'] = $uploded_file_name ; 
              
               if(isset($post['old_image_name']) && !empty($post['old_image_name'])) {
               		\File::delete($post['old_image_name']);
               }

          }

         $res = Blog::update_Or_insert($postData,$id);

            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Blog updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Blog added successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            } 

        return $res ? redirect('admin/blog')->with($flush_data['key'], $flush_data['msg'])  : redirect()->back()->with($flush_data['key'], $flush_data['msg']);

    }

     public function delete($id)
    {   
       if(!Blog::is_exists($id)){
         return redirect()->back()->with('error', 'Blog does not exist');
       }
        $postData = [
                    'is_deleted'=>1,
                    'deleted_at'=>date("Y-m-d H:i:s") ,
                    ];

         $res = Blog::update_Or_insert($postData,$id);

         if($res){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Blog deleted successfully' ;

            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }

            return  redirect()->back()->with($flush_data['key'], $flush_data['msg']);
    }

}
