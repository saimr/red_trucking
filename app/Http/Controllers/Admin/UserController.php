<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Rules\OldPasswordMatch;
use Illuminate\Support\Facades\Hash;
use App\User;
class UserController   extends Controller
{
    //

    public function index(Request $request)
    {   

    
        return view('admin.user.change_password');
    }

    public function update_password(Request $request)
    {   
     
      $flush_data =  array('key'=>'','msg'=>'') ;
      $post   = $request->all();
      $res = 0 ;
      $validation = [
                    'old_password' => ['required',new OldPasswordMatch($post['old_password'])],
                    'new_password' => 'min:8|required_with:confirm_password|same:confirm_password',
                    'confirm_password' => 'required|min:8',
                ] ;
        $customMessages = [] ;
        $request->validate($validation,$customMessages);

         try {
                  $session_user = auth()->user();
                  $user = User::find($session_user->id);
                  $user->password = Hash::make($post['new_password']);
                  $user->save();
                  $res = 1;
                  $flush_data['key'] = 'success' ;
                  $flush_data['msg'] = 'Password has been changed' ;
            } catch (Throwable $e) {
                report($e);
                $flush_data['key'] = 'error' ;
                $flush_data['msg'] = $e ;
            }

        
        return $res ? redirect('admin/changepassword')->with($flush_data['key'], $flush_data['msg'])  : redirect()->back()->with($flush_data['key'], $flush_data['msg']);


    }



  

    
}
