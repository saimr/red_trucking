<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;

class SocialController   extends Controller
{
    //

    public function index(Request $request)
    {   
        $social_media_data = MetaData::get_data_by_meta_type(config('constants.meta_type.social_media_link'));

        return view('admin.social.index',compact('social_media_data'));
    }



   

    public function update(Request $request)
    {   

      $flush_data =  array('key'=>'','msg'=>'') ;
      $post   = $request->all();

      $validation = [
                    'facebook_link' => 'required|url',
                    'twitter_link' => 'required|url',
                    'linkedin_link' => 'required|url',
                    'instagram_link' => 'required|url',
                ] ;

        $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;

        $customMessages = [ ];
          
       $request->validate($validation,$customMessages);
    
      $postData =  array(
                    'facebook_link'=>isset($post['facebook_link'])? $post['facebook_link'] : '',
                    'twitter_link'=>isset($post['twitter_link'])? $post['twitter_link'] : '',
                    'linkedin_link'=>isset($post['linkedin_link'])? $post['linkedin_link'] : '',
                    'instagram_link'=>isset($post['instagram_link'])? $post['instagram_link'] : '',
                    );

     

      
                $metaData =  array(
                                    'meta_type'=>config('constants.meta_type.social_media_link'),
                                    'meta_title'=> 'Social Media Link',
                                    'meta_data'=>json_encode($postData),
                            );
               
            $res = MetaData::update_Or_insert($metaData,$id);

            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Social media link updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Social media link added successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            } 

        return $res ? redirect('admin/social')->with($flush_data['key'], $flush_data['msg'])  : redirect()->back()->with($flush_data['key'], $flush_data['msg']);

    }


    
}
