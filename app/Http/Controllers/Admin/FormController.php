<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Form;

class FormController extends Controller
{
    //

     public function contact(Request $request)
    {   

    	 $form_data = Form::getAllFormtData(config('constants.form_type.contact'));
        return view('admin.form.contact',compact('form_data'));
    }

    public function contact_export(Request $request)
    {   

       $form_data = Form::getAllFormtData(config('constants.form_type.contact'));

           $delimiter = ",";
        $filename = "contact_forms_" . date('Y-m-d') . ".csv";
        
        //create a file pointer
        $f = fopen('php://memory', 'w');
        
        //set column headers
        $fields = array(
                      'Query_Date', 
                      'Name', 'Email', 
                      'Phone_No', 
                      'Best_Time', 
                      'Message', 
                      'Transport_Type', 
                      'Company_Name', 
                      'Commodity', 
                      'No_of_Loads', 
                      'Cargo_Weight', 
                      'Pick Up Address',
                      'City',
                      'Sate',
                      'Zip_Code',
                      'Intermodal_Equipment',
                      'Termination_Address',
                      'City',
                      'State',
                      'Zip_Code',
                      'Intermodal_Equipment',
                    );
        fputcsv($f, $fields, $delimiter);
         //output each row of the data, format line as csv and write to file pointer
       
          foreach ($form_data as $key => $value) {
            $lineData = [
                          date('d-M-Y',strtotime($value->created_at)),
                          $value->first_name.' '.$value->last_name,
                          $value->email,
                          $value->phone,
                          !empty($value->best_time) ?  date('h:i:A',$value->best_time) : '' ,
                          $value->message ?? '',
                          $value->transport_type == 1 ? ' Warehouse/Transloading':'Sales Quotes',
                          $value->company_name ?? '',
                          $value->commodity ?? '',
                          $value->number_of_loads ?? '',
                          $value->cargo_weight ?? '',
                          $value->pickup_address ?? '',
                          $value->pickup_city ?? '',
                          $value->pickup_state ?? '',
                          $value->pickup_zip_code ?? '',
                          $value->pickup_intermodal_equipment ?? '',
                          $value->destination_address ?? '',
                          $value->destination_city ?? '',
                          $value->destination_state ?? '',
                          $value->destination_zip_code ?? '',
                          $value->destination_intermodal_equipment ?? '',
                         ];
            fputcsv($f, $lineData, $delimiter);
          }
        
       
        
        //move back to beginning of file
        fseek($f, 0);
        
        //set headers to download file rather than displayed
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        
        //output all remaining data on a file pointer
        fpassthru($f);


    }

    

    public function career(Request $request)
    {   

      $form_data = Form::getAllFormtData(config('constants.form_type.career'));
      return view('admin.form.career',compact('form_data'));
    }

    public function career_export(Request $request)
    {   

       $form_data = Form::getAllFormtData(config('constants.form_type.career'));

           $delimiter = ",";
        $filename = "career_forms_" . date('Y-m-d') . ".csv";
        
        //create a file pointer
        $f = fopen('php://memory', 'w');
        
        //set column headers
        $fields = array('Query_Date', 'Name', 'Email', 'Phone_No', 'CDL', 'TWICE', '2 Year/Above_Experience','INMOTO or DRAYAGE');
        fputcsv($f, $fields, $delimiter);
         //output each row of the data, format line as csv and write to file pointer
       
          foreach ($form_data as $key => $value) {
            $lineData = [
                          date('d-M-Y',strtotime($value->created_at)),
                          $value->first_name.' '.$value->last_name,
                          $value->email,
                          $value->phone,
                          $value->is_cdl && !empty($value->is_cdl) ? 'Yes' :'No',
                          $value->is_twice && !empty($value->is_twice) ? 'Yes' :'No',
                          $value->is_driving_experience && !empty($value->is_driving_experience) ? 'Yes' :'No',
                          $value->is_inmoto_or_drayage && !empty($value->is_inmoto_or_drayage) ? 'Yes' :'No',
                         ];
            fputcsv($f, $lineData, $delimiter);
          }
        
       
        
        //move back to beginning of file
        fseek($f, 0);
        
        //set headers to download file rather than displayed
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        
        //output all remaining data on a file pointer
        fpassthru($f);


    }


    


}
