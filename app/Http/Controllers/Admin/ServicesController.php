<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Page;
use App\PageGallery;
use App\MetaData;

class ServicesController extends Controller
{
    //

     public function transport(Request $request)
    {   

    	 $transport_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_transport_page'));
    	
        return view('admin.services.transport',compact('transport_data'));
    }

     public function updatetransport(Request $request)
    {

      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
      $flush_data =  array('key'=>'','msg'=>'') ;
      
      $validation = [
                    'section_one_title' => 'required',
                    'section_one_description' => 'required',
                    'section_one_service_title' => 'required',
                    //'section_one_services[]' => 'required',
                    'section_two_title' => 'required',
                    'section_two_description' => 'required',
                    'section_three_title' => 'required',
                    'section_three_detail' => 'required',
                    //'section_three_points[]' => 'required',
                    'section_three_provide_title' => 'required',
                    //'section_three_features[]' => 'required',
                ];

      if(!$id || !empty($request->file('section_three_image'))){
           $validation['section_three_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=658,min_height=526' ;
         }

     $customMessages = [
                      'section_one_title.required' => 'The title field is required.',
                      'section_one_description.required' => 'The description field is required.',
                      'section_one_service_title.required' => 'The description field is required.',
                      //'section_one_services[].required' => 'The services field is required.',
                      'section_two_title.required' => 'The title field is required.',
                      'section_two_description.required' => 'The description field is required.',
                      'section_three_title.required' => 'The title field is required.',
                      'section_three_detail.required' => 'The detail field is required.',
                      'section_three_points[].required' => 'The points field is required.',
                      'section_three_provide_title.required' => 'The title field is required.',
                      //'section_three_features[].required' => 'The features field is required.',
                      'section_three_image.dimensions' => 'Select :attribute  must be at least min 658x526 pixels.',
                  ];
          
      $request->validate($validation,$customMessages); 

 
            $postData =  array(
                    'section_one_title'=>isset($post['section_one_title'])? $post['section_one_title'] : '',
                    'section_one_description'=>isset($post['section_one_description'])? $post['section_one_description'] : '',
                    'section_one_service_title'=>isset($post['section_one_service_title'])? $post['section_one_service_title'] : '',
                    'section_one_services'=>isset($post['section_one_services'])? json_encode($post['section_one_services']) : '',
                    'section_two_title'=>isset($post['section_two_title'])? $post['section_two_title'] : '',
                    'section_two_description'=>isset($post['section_two_description'])? $post['section_two_description'] : '',
                    'section_three_title'=>isset($post['section_three_title'])? $post['section_three_title'] : '',
                    'section_three_detail'=>isset($post['section_three_detail'])? $post['section_three_detail'] : '',
                    'section_three_points'=>isset($post['section_three_points'])? json_encode($post['section_three_points']) : '',
                    'section_three_provide_title'=>isset($post['section_three_provide_title'])? $post['section_three_provide_title'] : '',
                    'section_three_features'=>isset($post['section_three_features'])? json_encode($post['section_three_features']) : '',
            );

        $section_three_image = $request->file('section_three_image'); 
        if(isset($section_three_image) && !empty($section_three_image) ) {

              $fileName = time().'-.'.$section_three_image->getClientOriginalExtension();  
              $section_three_image->move('uploads/transport', $fileName);
              $uploded_file_name = 'uploads/transport/'.$fileName;
              $postData['section_three_image'] =  $uploded_file_name ;
               if(isset($post['old_section_three_image']) && !empty($post['old_section_three_image'])) {
                    \File::delete($post['old_section_three_image']);
               }
               
        } else {
              if(isset($post['old_section_three_image']) && !empty($post['old_section_three_image'])){
                $postData['section_three_image'] =  $post['old_section_three_image'] ;
              }
        }

         $metaData =  array(
                    'meta_type'=>config('constants.meta_type.service_transport_page'),
                    'meta_title'=> 'Service Transport  ',
                    'meta_data'=>json_encode($postData),
         );
           
         //pad($postData);
        $res = MetaData::update_Or_insert($metaData,$id);
   
            
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Transport updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Transport save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/services/transport')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.services.transport')->with($flush_data['key'], $flush_data['msg']);
    

    }

     public function warehousing(Request $request)
    {   

    	$warehousing_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_warehousing_page'));
      return view('admin.services.warehousing',compact('warehousing_data'));
    }

     public function updateswarehousing(Request $request)
    {
      
      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
      $flush_data =  array('key'=>'','msg'=>'') ;
      
     $validation = [
                    'section_one_title' => 'required',
                    'section_one_first_paragraphp' => 'required',
                    'section_one_second_paragraphp' => 'required',
                    'section_one_second_title' => 'required',
                    'section_one_second_first_paragraphp' => 'required',
                    'section_one_second_second_paragraphp' => 'required',
                    'section_two_title' => 'required',
                    'section_two_first_paragraphp' => 'required',
                    'section_two_second_paragraphp' => 'required',
                    'section_three_title' => 'required',
                    'section_three_description' => 'required',
                    'section_three_benefit_title' => 'required',
                    //'section_three_benefits[]' => 'required',
                ];


      if(!$id || !empty($request->file('section_three_image'))){
           $validation['section_three_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=694,min_height=526' ;
         }

     $customMessages = [
                      'section_one_title.required' => 'The title field is required.',
                      'section_one_first_paragraphp.required' => 'The description field is required.',
                      'section_one_second_paragraphp.required' => 'The description field is required.',
                      'section_one_second_title.required' => 'The services field is required.',
                      'section_one_second_first_paragraphp.required' => 'The title field is required.',
                      'section_one_second_second_paragraphp.required' => 'The description field is required.',
                      'section_two_title.required' => 'The title field is required.',
                      'section_two_first_paragraphp.required' => 'The detail field is required.',
                      'section_two_second_paragraphp.required' => 'The points field is required.',
                      'section_three_title.required' => 'The title field is required.',
                      'section_three_description.required' => 'The features field is required.',
                      'section_three_benefit_title.required' => 'The features field is required.',
                      //'section_three_benefits[].required' => 'The features field is required.',
                      'section_three_image.dimensions' => 'Select :attribute  must be at least min 694x526 pixels.',
                  ];
          
      $request->validate($validation); 

 
            $postData =  array(
                    'section_one_title'=>isset($post['section_one_title'])? $post['section_one_title'] : '',
                    'section_one_first_paragraphp'=>isset($post['section_one_first_paragraphp'])? $post['section_one_first_paragraphp'] : '',
                    'section_one_second_paragraphp'=>isset($post['section_one_second_paragraphp'])? $post['section_one_second_paragraphp'] : '',
                    'section_one_second_title'=>isset($post['section_one_second_title'])? $post['section_one_second_title'] : '',
                    'section_one_second_first_paragraphp'=>isset($post['section_one_second_first_paragraphp'])? $post['section_one_second_first_paragraphp'] : '',
                    'section_one_second_second_paragraphp'=>isset($post['section_one_second_second_paragraphp'])? $post['section_one_second_second_paragraphp'] : '',
                    'section_two_title'=>isset($post['section_two_title'])? $post['section_two_title'] : '',
                    'section_two_first_paragraphp'=>isset($post['section_two_first_paragraphp'])? $post['section_two_first_paragraphp'] : '',
                    'section_two_second_paragraphp'=>isset($post['section_two_second_paragraphp'])? $post['section_two_second_paragraphp']: '',
                    'section_three_title'=>isset($post['section_three_title'])? $post['section_three_title'] : '',
                    'section_three_description'=>isset($post['section_three_description'])? $post['section_three_description'] : '',
                    'section_three_benefit_title'=>isset($post['section_three_benefit_title'])? $post['section_three_benefit_title'] : '',
                    'section_three_benefits'=>isset($post['section_three_benefits'])? json_encode($post['section_three_benefits']) : '',
            );

        $section_three_image = $request->file('section_three_image'); 
        if(isset($section_three_image) && !empty($section_three_image) ) {

              $fileName = time().'-.'.$section_three_image->getClientOriginalExtension();  
              $section_three_image->move('uploads/warehousing', $fileName);
              $uploded_file_name = 'uploads/warehousing/'.$fileName;
              $postData['section_three_image'] =  $uploded_file_name ;
               if(isset($post['old_section_three_image']) && !empty($post['old_section_three_image'])) {
                    \File::delete($post['old_section_three_image']);
               }
               
        } else {
              if(isset($post['old_section_three_image']) && !empty($post['old_section_three_image'])){
                $postData['section_three_image'] =  $post['old_section_three_image'] ;
              }
        }

         $metaData =  array(
                    'meta_type'=>config('constants.meta_type.service_warehousing_page'),
                    'meta_title'=> 'Service Warehousing  ',
                    'meta_data'=>json_encode($postData),
         );
           
         //pad($postData);
        $res = MetaData::update_Or_insert($metaData,$id);
   
            
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Warehousing updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Warehousing  save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/services/warehousing')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.services.warehousing')->with($flush_data['key'], $flush_data['msg']);

    }

     public function logistics(Request $request)
    {   

    	 $logistics_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_logistics_page'));
      
        return view('admin.services.logistics',compact('logistics_data'));
    }

     public function updatelogistics(Request $request)
    {
      
      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
      $flush_data =  array('key'=>'','msg'=>'') ;
      
     $validation = [
                    'section_one_title' => 'required',
                    'section_one_first_paragraphp' => 'required',
                    'section_one_second_paragraphp' => 'required',
                    'section_one_second_title' => 'required',
                    'section_one_second_description' => 'required',
                    'section_two_first_title' => 'required',
                    'section_two_first_description' => 'required',
                    'section_two_second_title' => 'required',
                    'section_two_second_description' => 'required',
                    'section_third_first_title' => 'required',
                    'section_three_first_description' => 'required',
                    'section_three_second_title' => 'required',
                    'section_three_second_description' => 'required',
                    'section_third_third_title' => 'required',
                    'section_third_third_description' => 'required',
                ];


      if(!$id || !empty($request->file('section_three_image'))){
           $validation['section_three_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=610,min_height=526' ;
         }

     $customMessages = [
                      
                      'section_three_image.dimensions' => 'Select :attribute  must be at least min 610x526 pixels.',
                  ];
          
      $request->validate($validation); 

 
            $postData =  array(
                    'section_one_title'=>isset($post['section_one_title'])? $post['section_one_title'] : '',
                    'section_one_first_paragraphp'=>isset($post['section_one_first_paragraphp'])? $post['section_one_first_paragraphp'] : '',
                    'section_one_second_paragraphp'=>isset($post['section_one_second_paragraphp'])? $post['section_one_second_paragraphp'] : '',
                    'section_one_second_title'=>isset($post['section_one_second_title'])? $post['section_one_second_title'] : '',
                    'section_one_second_description'=>isset($post['section_one_second_description'])? $post['section_one_second_description'] : '',
                    'section_two_first_title'=>isset($post['section_two_first_title'])? $post['section_two_first_title'] : '',
                    'section_two_first_description'=>isset($post['section_two_first_description'])? $post['section_two_first_description'] : '',
                    'section_two_second_title'=>isset($post['section_two_second_title'])? $post['section_two_second_title'] : '',
                    'section_two_second_description'=>isset($post['section_two_second_description'])? $post['section_two_second_description'] : '',
                    'section_third_first_title'=>isset($post['section_third_first_title'])? $post['section_third_first_title'] : '',
                    'section_three_first_description'=>isset($post['section_three_first_description'])? $post['section_three_first_description'] : '',
                    'section_three_second_title'=>isset($post['section_three_second_title'])? $post['section_three_second_title'] : '',
                    'section_three_second_description'=>isset($post['section_three_second_description'])? $post['section_three_second_description'] : '',
                    'section_third_third_title'=>isset($post['section_third_third_title'])? $post['section_third_third_title'] : '',
                    'section_third_third_description'=>isset($post['section_third_third_description'])? $post['section_third_third_description'] : '',
            );

        $section_three_image = $request->file('section_three_image'); 
        if(isset($section_three_image) && !empty($section_three_image) ) {

              $fileName = time().'-.'.$section_three_image->getClientOriginalExtension();  
              $section_three_image->move('uploads/logistics', $fileName);
              $uploded_file_name = 'uploads/logistics/'.$fileName;
              $postData['section_three_image'] =  $uploded_file_name ;
               if(isset($post['old_section_three_image']) && !empty($post['old_section_three_image'])) {
                    \File::delete($post['old_section_three_image']);
               }
               
        } else {
              if(isset($post['old_section_three_image']) && !empty($post['old_section_three_image'])){
                $postData['section_three_image'] =  $post['old_section_three_image'] ;
              }
        }

         $metaData =  array(
                    'meta_type'=>config('constants.meta_type.service_logistics_page'),
                    'meta_title'=> 'Service Logistics  ',
                    'meta_data'=>json_encode($postData),
         );
           
         //pad($postData);
        $res = MetaData::update_Or_insert($metaData,$id);
   
            
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Logistics updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Logistics save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/services/logistics')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.services.logistics')->with($flush_data['key'], $flush_data['msg']);

    

    }

      public function transloading(Request $request)
    {   

    	  $transloading_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_transloading_page'));
        return view('admin.services.transloading',compact('transloading_data'));
    }

     public function updatetransloading(Request $request)
    {
      
      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
      $flush_data =  array('key'=>'','msg'=>'') ;
      
     $validation = [
                    'section_one_title' => 'required',
                    'section_one_description' => 'required',
                    'section_two_title' => 'required',
                    //'section_two_services[]' => 'required',
                    'section_third_feature_title' => 'required',
                    //'section_third_features[]' => 'required',
                    'section_third_title' => 'required',
                    'section_third_description' => 'required',
                ];


      if(!$id || !empty($request->file('section_third_image'))){
           $validation['section_third_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=698,min_height=526' ;
         }

     $customMessages = [
                      
                      'section_third_image.dimensions' => 'Select :attribute  must be at least min 698x526 pixels.',
                  ];
          
      $request->validate($validation); 

 
            $postData =  array(
                    'section_one_title'=>isset($post['section_one_title'])? $post['section_one_title'] : '',
                    'section_one_description'=>isset($post['section_one_description'])? $post['section_one_description'] : '',
                    'section_two_title'=>isset($post['section_two_title'])? $post['section_two_title'] : '',
                    'section_two_services'=>isset($post['section_two_services'])? json_encode($post['section_two_services']) : '',
                    'section_third_feature_title'=>isset($post['section_third_feature_title'])? $post['section_third_feature_title'] : '',
                    'section_third_features'=>isset($post['section_third_features'])? json_encode($post['section_third_features']) : '',
                    'section_third_title'=>isset($post['section_third_title'])? $post['section_third_title'] : '',
                    'section_third_description'=>isset($post['section_third_description'])? $post['section_third_description'] : '',
            );

        $section_third_image = $request->file('section_third_image'); 
        if(isset($section_third_image) && !empty($section_third_image) ) {

              $fileName = time().'-.'.$section_third_image->getClientOriginalExtension();  
              $section_third_image->move('uploads/transloading', $fileName);
              $uploded_file_name = 'uploads/transloading/'.$fileName;
              $postData['section_third_image'] =  $uploded_file_name ;
               if(isset($post['old_section_third_image']) && !empty($post['old_section_third_image'])) {
                    \File::delete($post['old_section_third_image']);
               }
               
        } else {
              if(isset($post['old_section_third_image']) && !empty($post['old_section_third_image'])){
                $postData['section_third_image'] =  $post['old_section_third_image'] ;
              }
        }

         $metaData =  array(
                    'meta_type'=>config('constants.meta_type.service_transloading_page'),
                    'meta_title'=> 'Service Transloading  ',
                    'meta_data'=>json_encode($postData),
         );
           
         //pad($postData);
        $res = MetaData::update_Or_insert($metaData,$id);
   
            
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Transloading updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Transloading save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/services/transloading')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.services.transloading')->with($flush_data['key'], $flush_data['msg']);
    

    }

}
