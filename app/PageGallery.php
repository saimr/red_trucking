<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class PageGallery extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'page_id',
        'type',
        'name',
    ];

       protected $table = 'page_gallery';

    public static function update_Or_insert($postData,$id=0){
      
      if($id){
         $postData['updated_at']  =  date("Y-m-d H:i:s") ;
         $result = DB::table('page_gallery')->where('id',$id)->update($postData);  
      } else {
        $postData['created_at']  =  date("Y-m-d H:i:s") ;
        $result = DB::table('page_gallery')->insert($postData);         
      }
      
       return $result;
    }

    public static function get_data_by_page_id($page_id,$type,$status=1){

       $result = DB::table('page_gallery')->
                    select('page_gallery.*')->
                    where('page_gallery.page_id',$page_id)->
                    where('page_gallery.type',$type)->
                    where('page_gallery.status',$status)->
                    where('page_gallery.is_deleted',0)->first();

       return $result;
    }

    public static function get_images_by_page_id($page_id,$type,$status=1){

       $result = DB::table('page_gallery')->
                    select('page_gallery.*')->
                    where('page_gallery.page_id',$page_id)->
                    where('page_gallery.type',$type)->
                    where('page_gallery.status',$status)->
                    where('page_gallery.is_deleted',0)->get();

       return $result;
    }

    public static function get_right_images_by_page_id($page_id,$type,$status=1){

       $result = DB::table('page_gallery')->
                    select('page_gallery.*')->
                    where('page_gallery.page_id',$page_id)->
                    where('page_gallery.type',$type)->
                    where('page_gallery.status',$status)->
                    where('page_gallery.is_deleted',0)->skip(1)->first();

       return $result;
    }

}
