<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

//Route::redirect('/', '/login');


 Route::resource('/', 'SiteController');
 Route::get('thankyou', 'SiteController@thank_you');

 Route::get('about', 'AboutusController@index');
 Route::get('about/safety', 'AboutusController@safety');
 Route::get('about/sustainability', 'AboutusController@sustainability');
 Route::resource('contact', 'ContactController');
 Route::post('contact/save', 'ContactController@save');
 Route::get('blogs', 'BlogController@index');
 Route::get('blogs/detail/{blog_url}', 'BlogController@detail');
 Route::get('driver', 'DriverController@index');
 Route::post('driver/save', 'DriverController@save');
 Route::get('service/transport', 'ServiceController@transport');
 Route::get('service/warehousing', 'ServiceController@warehousing');
 Route::get('service/logistics', 'ServiceController@logistics');
 Route::get('service/transloading', 'ServiceController@transloading');



////// Admin Route Start  ////////////

Route::redirect('/home', '/admin');

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');

    /////////  Banner Module ////
    Route::get('banner', 'BannerController@index');
    Route::post('banner/update', 'BannerController@update');
    Route::get('banner/delete/{id}', 'BannerController@delete');

    /////////  Home Page Module ////
    Route::get('homepage', 'HomePageController@index');
    Route::post('homepage/update', 'HomePageController@update');

    Route::get('homepage/addclient', 'HomePageController@addclient');
    Route::get('homepage/editclient/{id}', 'HomePageController@editclient');
    Route::post('homepage/updateclient', 'HomePageController@updateclient');

    /////////  Testimonial Module ////
    Route::get('testimonial', 'TestimonialController@index');
    Route::get('testimonial/add/', 'TestimonialController@add');
    Route::get('testimonial/edit/{id}', 'TestimonialController@edit');
    Route::post('testimonial/update', 'TestimonialController@update');
    Route::get('testimonial/delete/{id}', 'TestimonialController@delete');


    /////////  About Us Module ////
    Route::get('about', 'AboutController@index');
    Route::post('about/updateabout', 'AboutController@updateabout');
    Route::get('about/addmember', 'AboutController@addmember');
    Route::get('about/editmember/{id}', 'AboutController@editmember');
    Route::post('about/updatemember', 'AboutController@updatemember');

    Route::get('about/safety', 'AboutController@safety');
    Route::post('about/updatesafety', 'AboutController@updatesafety');

    Route::get('about/sustainability', 'AboutController@sustainability');
    Route::post('about/updatesustainability', 'AboutController@updatesustainability');

     /////////  Contact Us Module ////
    Route::get('contactus', 'ContactController@index');
    Route::post('contactus/update', 'ContactController@update');

    /////////  Services Module ////
    Route::get('services/transport', 'ServicesController@transport');
    Route::post('services/updatetransport', 'ServicesController@updatetransport');

    Route::get('services/warehousing', 'ServicesController@warehousing');
    Route::post('services/updateswarehousing', 'ServicesController@updateswarehousing');

    Route::get('services/logistics', 'ServicesController@logistics');
    Route::post('services/updatelogistics', 'ServicesController@updatelogistics');

    Route::get('services/transloading', 'ServicesController@transloading');
    Route::post('services/updatetransloading', 'ServicesController@updatetransloading');

    /////////  Red Deriver Module ////
    Route::get('driver', 'DriverController@index');
    Route::post('driver/update', 'DriverController@update');
    Route::get('driver/page_gallery_delete/{id}', 'DriverController@page_gallery_delete');

    /////////  Partner Module ////

    Route::get('partner', 'PartnerController@index');
    Route::post('partner/update', 'PartnerController@update');

    /////////  Red Trucking Form Module ////
    Route::get('form/contact', 'FormController@contact');
    Route::get('form/contact/export', 'FormController@contact_export');
    Route::get('form/career', 'FormController@career');
    Route::get('form/career/export', 'FormController@career_export');

    ////////  Blog Form Module ////
    Route::get('blog/', 'BlogController@index');
    Route::get('blog/add', 'BlogController@add');
    Route::post('blog/update', 'BlogController@update');
    Route::get('blog/edit/{id}', 'BlogController@edit');
    Route::get('blog/delete/{id}', 'BlogController@delete');

    ////////  Social Media Module ////
    Route::get('social/', 'SocialController@index');
    Route::post('social/update', 'SocialController@update');

    ////////  Change Password ////
    Route::get('changepassword/', 'UserController@index');
     Route::post('update_password', 'UserController@update_password');
});

////// Admin Route End  ////////////



