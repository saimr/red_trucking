// Auther by Mohd AJAM Ansari

$(document).ready(function () {

  refersh_capture () ;
    $(".is_alphabet").keypress(function (e) {
        var keyCode = e.keyCode || e.which;

        $("#lblError").html("");

        //Regex for Valid Characters i.e. Alphabets.
        var regex = /^[A-Za-z ]+$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));

        return isValid;
      });

      $(".is_number").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          //display error message
          //$("#errmsg").html("Digits Only").show().fadeOut("slow");
          return false;
        }

      });
        $.validator.addMethod("validateCapture", function(value, element) {
          console.log('calling');
          let  first_number = parseInt($('.first_number').val()); 
          let  second_number =  parseInt($('.second_number').val()); 
          let  result =  parseInt($('#result').val()); 
            return result == (first_number+second_number) ? true :'' ;
            //return this.optional(element) || value != 'default' ;
        }, "Invalid Capture");
          

    $("#contact_us_form").validate({
            rules: {
              first_name : {
                required: true,
              },
              last_name : {
                required: true,
              },
              email : {
                required: true,
                email: true
              },
              phone : {
                required: true,
                minlength: 10,
                maxlength: 10
              },
              // transport_type[] : {
              //   required: true,
              // },

              best_time_hour : {
                required: true,
              },
              best_time_minute : {
                required: true,
              },
              best_time : {
                required: true,
              },
              result : {
                required: true,
                validateCapture:true,
              }
            }
            ,errorPlacement: function(error, element) {
                    if (element.attr("name") == "transport_type" )  //Id of input field
                        error.appendTo('.transport_type_error');
                     if (element.attr("name") == "result" )  //Id of input field
                        error.appendTo('.result_error');
            }
    });

    function refersh_capture () { 
       let first_number = Math.ceil(Math.random() * 10);
       let second_number = Math.ceil(Math.random() * 10);        
       $('.first_number').val(first_number); 
       $('.second_number').val(second_number); 

    }

    $(".refersh_capture").click(function () {
      refersh_capture() ;
    });

});

