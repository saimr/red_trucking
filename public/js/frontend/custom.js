//Dark Navbar

$(function () {
    var topheight = window.pageYOffset;
    if (topheight >= 40) {
        $(".redhome_header").addClass("darkHeader");
    } else {
        $(".redhome_header").removeClass("darkHeader");
    }
})

//Header Navbar On Scroll

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 40) {
        $(".redhome_header").addClass("darkHeader");
    } else {
        $(".redhome_header").removeClass("darkHeader");
    }
});

//Navbar menu close outside click
$(document).ready(function () {
    $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $(".navbar-collapse").hasClass("show");
        if (_opened === true && !clickover.hasClass("navbar-toggler")) {
            $("button.navbar-toggler").click();
        }
    });
});

// Back to top button
$(".back-top a").hide();
$(window).scroll(function () {
  if ($(this).scrollTop() > 100) {
    $(".back-top a").fadeIn();
  } else {
    $(".back-top a").fadeOut();
  }
});
$(".back-top a").click(function () {
  $("html, body").animate({ scrollTop: 0 }, 800);
  return false;
});

//Video Play

// $('.hmabvideo').parent().click(function () {
//     if ($(this).children(".hmabvideo").get(0).paused) {
//         $(this).children(".hmabvideo").get(0).play();
//         // $(this).children(".playpause").fadeOut();
//     } else {
//         $(this).children(".hmabvideo").get(0).pause();
//         // $(this).children(".playpause").fadeIn();
//     }
// });



//Resources Slider
//$(document).ready(function () {
//    var $slider = $('.slider');
//    var $progressBar = $('.progress');
//    var $progressBarLabel = $('.slider__label');
//
//    $slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
//        var calc = ((nextSlide) / (slick.slideCount - 1)) * 100;
//
//        $progressBar
//            .css('background-size', calc + '% 100%')
//            .attr('aria-valuenow', calc);
//
//        $progressBarLabel.text(calc + '% completed');
//    });
//
//    $slider.slick({
//        slidesToShow: 3,
//        slidesToScroll: 1,
//        autoplay: true,
//        speed: 400,
//        dots: false,
//        prevArrow: '<span class="prevs_arrow"><img src="svg/forward.svg"></span>',
//        nextArrow: '<span class="nxt_arrow"><img src="svg/forward.svg"></span>',
//        responsive: [{
//                breakpoint: 1024,
//                settings: {
//                    slidesToShow: 3,
//                    slidesToScroll: 1
//
//                }
//                    },
//            {
//                breakpoint: 991,
//                settings: {
//                    slidesToShow: 2,
//                    slidesToScroll: 1
//                }
//                    },
//            {
//                breakpoint: 480,
//                settings: {
//                    slidesToShow: 1,
//                    slidesToScroll: 1
//                }
//                    }
//                ]
//
//    });
//});

//contact Form
var a, b, c,
    submitContent,
    captcha,
    locked,
    validSubmit = false,
    timeoutHandle;

// Generating a simple sum (a + b) to make with a result (c)
function generateCaptcha() {
    a = Math.ceil(Math.random() * 10);
    b = Math.ceil(Math.random() * 10);
    c = a + b;
    submitContent = '<span>' + a + '</span> + <span>' + b + '</span>' +
        ' = <input class="submit__input" type="text" maxlength="2" size="2" required />';
    $('.submit__generated').html(submitContent);

    init();
}


// Check the value 'c' and the input value.
function checkCaptcha() {
    if (captcha === c) {
        // Pop the green valid icon
        $('.submit__generated')
            .removeClass('unvalid')
            .addClass('valid');
        $('.submit').removeClass('overlay');
        $('.submit__overlay').fadeOut('fast');

        $('.submit__error').addClass('hide');
        $('.submit__error--empty').addClass('hide');
        validSubmit = true;
    } else {
        if (captcha === '') {
            $('.submit__error').addClass('hide');
            $('.submit__error--empty').removeClass('hide');
        } else {
            $('.submit__error').removeClass('hide');
            $('.submit__error--empty').addClass('hide');
        }
        // Pop the red unvalid icon
        $('.submit__generated')
            .removeClass('valid')
            .addClass('unvalid');
        $('.submit').addClass('overlay');
        $('.submit__overlay').fadeIn('fast');
        validSubmit = false;
    }
    return validSubmit;
}

function unlock() {
    locked = false;
}


// Refresh button click - Reset the captcha
$('.submit__control i.fa-sync').on('click', function () {
    if (!locked) {
        locked = true;
        setTimeout(unlock, 500);
        generateCaptcha();
        setTimeout(checkCaptcha, 0);
    }
});


// init the action handlers - mostly useful when 'c' is refreshed
function init() {
    $('form').on('submit', function (e) {
        e.preventDefault();
        if ($('.submit__generated').hasClass('valid')) {
            // var formValues = [];
            captcha = $('.submit__input').val();
            if (captcha !== '') {
                captcha = Number(captcha);
            }

            checkCaptcha();

            if (validSubmit === true) {
                validSubmit = false;
                // Temporary direct 'success' simulation
                submitted();
            }
        } else {
            return false;
        }
    });


    // Captcha input result handler
    $('.submit__input').on('propertychange change keyup input paste', function () {
        // Prevent the execution on the first number of the string if it's a 'multiple number string'
        // (i.e: execution on the '1' of '12')
        window.clearTimeout(timeoutHandle);
        timeoutHandle = window.setTimeout(function () {
            captcha = $('.submit__input').val();
            if (captcha !== '') {
                captcha = Number(captcha);
            }
            checkCaptcha();
        }, 150);
    });


    // Add the ':active' state CSS when 'enter' is pressed
    //    $('body')
    //        .on('keydown', function (e) {
    //            if (e.which === 13) {
    //                if ($('.submit-form').hasClass('overlay')) {
    //                    checkCaptcha();
    //                } else {
    //                    $('.submit-form').addClass('enter-press');
    //                }
    //            }
    //        })
    //        .on('keyup', function (e) {
    //            if (e.which === 13) {
    //                $('.submit-form').removeClass('enter-press');
    //            }
    //        });

    // Submit white overlay click
    //    $('.submit-form-overlay').on('click', function () {
    //        checkCaptcha();
    //    });
}

generateCaptcha();



//Contact form Sales
$(".cardinfobox").hide();

$("#optradio2").click(function () {
    if ($(this).is(":checked")) {
        $(".cardinfobox").show();
    } else {
        $(".cardinfobox").hide();
    }
});
$("#optradio1").click(function () {
    if ($(this).is(":checked")) {
        $(".cardinfobox").hide();
    }
});






// $(document).ready(function(){
//     $(".contact-form .form-group .submit").submit(function() {
//         $('html, body').animate({
//           scrollTop: $(".contact-sendmessage").offset().top
//        }, 1000);
//   });

// });


