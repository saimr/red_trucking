<!doctype html>
<html lang="en">

<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KZNW47R');</script>
    <!-- End Google Tag Manager -->


    <!-- Required meta tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">   
    <title>@php echo isset($page_title) ? $page_title : config('constants.website_name') ; @endphp</title>
    <meta name="google-site-verification" content="03hhLUrUD-jtDcvtDTeSkxKGtzZJaMzHpR-Hkv5Oino" />
    <meta name="keyword" content="@php echo isset($keyword) ? $keyword : '' ;  @endphp">
    <meta name="description" content="@php echo isset($description) ? $description : '' ;  @endphp">

    <meta property="og:title" content="@php echo isset($og_meta['title']) ? $og_meta['title'] : '' ;  @endphp">
    <meta property="og:site_name" content="@php echo isset($og_meta['site_name']) ? $og_meta['site_name'] : '' ;  @endphp">
    <meta property="og:url" content="@php echo isset($og_meta['url']) ? URL::to($og_meta['url']) : '' ;  @endphp">
    <meta property="og:description" content="@php echo isset($og_meta['description']) ? $og_meta['description'] : '' ;  @endphp">
    <meta property="og:type" content="website">
    <meta property="og:image" content="@php echo isset($og_meta['image']) ? URL::to($og_meta['image']) : '' ;  @endphp">


     <link href="{{ URL::to(config('constants.website_favicon_image')) }}" rel="icon" />
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/frontend/main.css') }}">

    @yield('styles')

    <script type="text/javascript">
      var base_url = '{{ env('APP_URL') }}';
    </script>

    <script>
      gtag('event', 'conversion', {'send_to': 'AW-695389019/VNkOCKWGme8BENuWy8sC'});
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-185366974-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-185366974-1');
    </script>
    <!-- Global site tag (gtag.js) - Google Ads: 695389019 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-695389019"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-695389019');
    </script>

</head>

<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KZNW47R"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

	@include('partials.frontend.header')
	@yield('content')
	@include('partials.frontend.footer')


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('js/frontend/jquery.min.js') }}"></script>
    <script src="{{ asset('js/frontend/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/frontend/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/frontend/common.js?reload=true') }}"></script>
    <script src="{{ asset('js/frontend/custom.js?reload=true') }}"></script>
    <script src="{{ asset('js/frontend/wow.js') }}"></script>
    <script src="{{ asset('js/frontend/jquery.mCustomScrollbar.js') }}"></script>
    
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script>
        new WOW().init();

    </script>


    <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjHH2urSZPwU_jFK8czhCpHpRVE8HV1co&callback=initMap"></script>
 -->

    <script type="text/javascript">
    //RED driver slider
        $(document).ready(function() {


            //Happy Client home page
            $('.home-happclient').slick({
                dots: false,
                infinite: true,
                autoplay: true,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplaySpeed: 1000,
                pauseOnHover: true,
                arrows: true,
                prevArrow: '<span class="testi-arrow-left"><i class="fas fa-chevron-left"></i></span>',
                nextArrow: '<span class="testi-arrow-right"><i class="fas fa-chevron-right"></i></span>',
                responsive: [{
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            arrows: false,
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

        //Home Testimonial
            $('.home-testimonial').slick({
                dots: false,
                infinite: true,
                autoplay: true,
                speed: 500,
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplaySpeed: 2500,
                pauseOnHover: true,
                arrows: true,
                prevArrow: '<span class="testi-arrow-left"><i class="fas fa-chevron-left"></i></span>',
                nextArrow: '<span class="testi-arrow-right"><i class="fas fa-chevron-right"></i></span>',
                responsive: [{
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            arrows: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });



            $('.items-slider-container .slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.js-items-slider-container .slider-nav',
                dots: false,
                autoplay: true,
                autoplaySpeed: 4000,
                pauseOnHover: true
            });
            $('.items-slider-container .slider-nav').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.js-items-slider-container .slider-for',
                dots: true,
                autoplay: true,
                autoplaySpeed: 4000,
                pauseOnHover: true
            });



        });

        

        // Wait for the DOM to be ready
        $(function() {
            // Initialize form validation on the registration form.
            // It has the name attribute "registration"
            $("form[name='driverregistration']").validate({
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                    first_name: "required",
                    //last_name: "required",
                    is_cdl: "required",
                    is_driving_experience: "required",
                    is_twice: "required",
                    phone: {
                        required: true,
                        minlength: 10,
                        maxlength: 10
                    },
                    email: {
                        required: true,
                        // Specify that email should be validated
                        // by the built-in "email" rule
                        email: true
                    }
                },

                // Specify validation error messages
                messages: {
                    first_name: "Please enter your first name",
                    last_name: "Please enter your last name",
                    email:{
                        required:"Please enter your email address", 
                        required:"Please enter your email address", 
                    },
                    phone: {
                            required:"Please enter phone number",    
                            minlength:"Please enter valid phone number",    
                            maxlength:"Please enter valid phone number",    
                    },
                    is_cdl: "Please select the option",
                    is_driving_experience: "Please select the option",
                    is_twic: "Please select the option"
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {
                    form.submit();
                }
            });

            $.validator.addMethod("contactvalidateCapture", function(value, element) {
          console.log('calling');
          let  first_number = parseInt($('.first_number').val()); 
          let  second_number =  parseInt($('.second_number').val()); 
          let  result =  parseInt($('#result').val()); 
            if(result == (first_number+second_number) ) {
                    $('.submit__generated1').removeClass('unvalid') ;
                    $('.submit__generated1').addClass('valid') ;
                return true ;
            } else {
                $('.submit__generated1').removeClass('valid') ;
                $('.submit__generated1').addClass('unvalid') ;
                return '' ;
            }
            //return result == (first_number+second_number) ? true :'' ;
            //return this.optional(element) || value != 'default' ;
        }, "Invalid Capture");



            $("form[name='contactregister']").validate({
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                    first_name: "required",
                    // best_time_hour: "required",
                    // best_time_minute: "required",
                    // best_time: "required",
                    phone: {
                        required: true,
                        minlength: 10,
                        maxlength: 10,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    capture : {
                        required: true,
                        contactvalidateCapture:true,
                    }
                },
                messages: {
                    first_name: "Please enter your first name",
                    last_name: "Please enter your last name",
                    email:{
                        required:"Please enter your email address", 
                        required:"Please enter your email address", 
                    },
                    phone: {
                            required:"Please enter phone number",    
                            minlength:"Please enter valid phone number",    
                            maxlength:"Please enter valid phone number",    
                    },
                    best_time_hour: "Please select the time",
                    best_time_minute: "Please select the time",
                    best_time: "Please select the time"
                },
                
                submitHandler: function(form) {
                    form.submit();
                }
            });

            $("#optradio2").click(function() {
                if ($(this).is(":checked")) {
                    $(".cardinfobox").show();
                    $("form[name='contactregister']").validate();
                    $('#compname').rules('add', {
                        required: true
                    });
                    $('#commodity').rules('add', {
                        required: true
                    });
                } else {
                    $(".cardinfobox").hide();
                    $('#compname').rules('remove', 'required');
                    $('#commodity').rules('remove', 'required');
                }
            });


        });

        function numberCheck() {
            var ncheck = document.driverregistration.phone.value;
            if (isNaN(ncheck)) {
                document.driverregistration.phone.value = '';
                return false;
            }
        }

        function firstName() {
            var firstalphabet = document.driverregistration.first_name.value;
            if (!isNaN(firstalphabet)) {
                document.driverregistration.first_name.value = '';
                return false;
            }
        }

        function lastName() {
            var lastalphabet = document.driverregistration.last_name.value;
            if (!isNaN(lastalphabet)) {
                document.driverregistration.last_name.value = '';
                return false;
            }
        }

        $(".cardinfobox").hide();


        $("#optradio1").click(function() {
            if ($(this).is(":checked")) {
                $(".cardinfobox").hide();
            }
        });

// custom scroll bar
        $(".slide-testimonial .testi-cont").mCustomScrollbar({
			theme:"dark"
		});
        
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-ZY3GM69DYD"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-ZY3GM69DYD');
</script>

<!-- LiveSession.io code -->
    <script type="text/javascript">
      !function(w, d, t, u) {
          if (w.__ls) return; var f = w.__ls = function() { f.push ? f.push.apply(f, arguments) : f.store.push(arguments)};
          if (!w.__ls) w.__ls = f; f.store = []; f.v = "1.0";
  
          var ls = d.createElement(t); ls.async = true; ls.src = u;
          var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(ls, s);
      }(window, document, 'script', ('https:' == window.location.protocol ? 'https://' : 'http://') + 'cdn.livesession.io/track.js');
  
      __ls("init", "7ccbde80.094c94e2", { keystrokes: false });
      __ls("newPageView");
    </script>
    <!-- END LiveSession.io code -->

<!-- Default Statcounter code for RED Trucking https://www.redtrucking.com/ -->
<script type="text/javascript">
var sc_project=12446313; 
var sc_invisible=1; 
var sc_security="dae56e02"; 
var sc_https=1; 
</script>
<script type="text/javascript"
src="https://www.statcounter.com/counter/counter.js" async></script>
<noscript><div class="statcounter"><a title="Web Analytics"
href="https://statcounter.com/" target="_blank"><img class="statcounter"
src="https://c.statcounter.com/12446313/0/dae56e02/1/" alt="Web
Analytics"></a></div></noscript>
<!-- End of Statcounter Code -->

<script type='application/ld+json'> 
{
  "@context": "http://www.schema.org",
  "@type": "AutomotiveBusiness",
  "name": "RED Trucking",
  "url": "https://www.redtrucking.com/",
  "logo": "https://www.redtrucking.com/images/frontend/site-logo.png",
  "image": "https://www.redtrucking.com/uploads/blogs/1607682404.jpg",
  "description": "Founded in 2004, RED Trucking has been providing comprehensive logistics, shipping and warehousing services to a wide range of domestic and international clients. ",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "158 Paris St",
    "addressLocality": "Newark",
    "addressRegion": "New Jersey",
    "postalCode": "07105",
    "addressCountry": "United States"
  },
  "hasMap": "https://goo.gl/maps/eaTkamBHPk4o55KUA",
  "openingHours": "Mo, Tu, We, Th, Fr, Sa, Su -",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "973-732-1333"
  }
}
 </script>



</body>

</html>