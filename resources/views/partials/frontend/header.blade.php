<header class="fixed-top redhome_header">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg  navbar-light">
                <a class="navbar-brand" href="{{ URL::to('/') }}">
                    <img src="{{ URL::to(asset('images/frontend/site-logo.png')) }} " class="img-fluid">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown {{ request()->is('about') || request()->is('about/safety') ? 'active' : '' }} " >
                            <a class="nav-link" href="{{ URL::to('about') }}">About</a>
                            <div class="dropdown-menu-cus" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item {{ request()->is('about/safety') ? 'active' : '' }}" href="{{ URL::to('about/safety') }}">Safety</a>
                                <a class="dropdown-item {{ request()->is('about/sustainability') ? 'active' : '' }}" href="{{ URL::to('about/sustainability') }}">Sustainability</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown {{ request()->is('service/transport') || request()->is('service/warehousing') || request()->is('service/logistics') || request()->is('service/transloading')  ? 'active' : '' }}">
                            <a class="nav-link" href="javascript:">
                                Services
                            </a>
                            <div class="dropdown-menu-cus" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item {{ request()->is('service/transport') ? 'active' : '' }}" href="{{ URL::to('service/transport')}}">Transport</a>
                                <a class="dropdown-item {{ request()->is('service/warehousing') ? 'active' : '' }}" href="{{ URL::to('service/warehousing')}}">Warehousing</a>
                                <a class="dropdown-item {{ request()->is('service/logistics') ? 'active' : '' }}" href="{{ URL::to('service/logistics')}}">Logistics</a>
                                <a class="dropdown-item {{ request()->is('service/transloading') ? 'active' : '' }}" href="{{ URL::to('service/transloading')}}">Transloading</a>
                            </div>
                        </li>
                        <li class="nav-item {{ request()->is('driver') ? 'active' : '' }}">
                            <a class="nav-link " href="{{ URL::to('driver')}}">Red Drivers</a>
                        </li>
                        <li class="nav-item {{ request()->is('blogs') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ URL::to('blogs') }}">Blogs</a>
                        </li>
                        <li class="nav-item {{ request()->is('contact') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ URL::to('contact') }}">Contact us</a>
                        </li>
                        
                    </ul>
                </div>
                <div class="client-login">
                    <a class="nav-link" href="http://red.activetrac.net/" target="_blank">Client Login</a>
                </div>
            </nav>
        </div>
</header>