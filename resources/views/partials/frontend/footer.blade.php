
@php 
$social_media_link = get_social_media_links() ;
$contact_us_meta = get_contact_us_data();

@endphp

<!-- Footer   -->
    <footer>
        <!-- <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="footer_logo">
                            <a href="{{ URL::to('/') }}">
                                <img src="{{ URL::to(asset('images/frontend/site-logo.png')) }} " class="img-fluid">
                            </a>

                        </div>
                    </div>
                    <div class="col-md-6  d-flex align-items-center">
                        <div class="footer_menu">
                            <ul>
                                <li><a href="{{ URL::to('about') }}">About</a></li>
                                <li><a href="{{ URL::to('service/transport')}}">Services</a></li>
                                <li><a href="{{ URL::to('driver')}}">RED Drivers</a></li>
                                <li><a href="{{ URL::to('blogs') }}">Blogs</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 d-flex align-items-center flex-row-reverse">
                        <div class="footer_social">
                            <ul>
                                <li><a href="{{ $social_media_link['facebook_link'] ?? '' }}" target="_blank">Facebook</a></li>
                                <li><a href="{{ $social_media_link['instagram_link'] ?? '' }}" target="_blank">Instagram</a></li>
                                <li><a href="{{ $social_media_link['twitter_link'] ?? '' }}" target="_blank">Twitter</a></li>
                                <li><a href="{{ $social_media_link['linkedin_link'] ?? '' }}" target="_blank">Linkedin</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="footer_logo">
                            <a href="{{ URL::to('/') }}">
                                <img src="{{ URL::to(asset('images/frontend/site-logo.png')) }} " class="img-fluid">
                            </a>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer_menu">
                            <h4>Contact Information</h4>
                            <address>
                            <p><b>Email :</b><a href="mailto:{{ isset($contact_us_meta->red_headquarter_email) ? $contact_us_meta->red_headquarter_email : '' }}">{{ isset($contact_us_meta->red_headquarter_email) ? $contact_us_meta->red_headquarter_email : '' }}</a></p>
                            <p><b>Phone :</b><a href="tel:{{ isset($contact_us_meta->red_headquarter_phone) ? str_replace('-','',$contact_us_meta->red_headquarter_phone ): '' }}">{{ isset($contact_us_meta->red_headquarter_phone) ? $contact_us_meta->red_headquarter_phone : '' }}</a></p>
                            <p><b>Location :</b> <a href="{{  isset($contact_us_meta->red_headquarter_email_google_map_link) ? $contact_us_meta->red_headquarter_email_google_map_link : 'javscript:'}}" target="_blank">{{ isset($contact_us_meta->red_headquarter_address) ? $contact_us_meta->red_headquarter_address : '' }} {{  isset($contact_us_meta->red_headquarter_address1) ? $contact_us_meta->red_headquarter_address1 : '' }}</a>
                            </p>
                                
                            </address>
                            
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer_social">
                        <h4>Services</h4>
                            <ul>
                                <li><a href="{{ URL::to('about') }}">About</a></li>
                                <li><a href="{{ URL::to('service/transport')}}">Services</a></li>
                                <li><a href="{{ URL::to('driver')}}">RED Drivers</a></li>
                                <li><a href="{{ URL::to('blogs') }}">Blogs</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer_social">
                        <h4>Links</h4>
                            <ul>
                                <li><a href="{{ $social_media_link['facebook_link'] ?? '' }}" target="_blank">Facebook</a></li>
                                <li><a href="{{ $social_media_link['instagram_link'] ?? '' }}" target="_blank">Instagram</a></li>
                                <li><a href="{{ $social_media_link['twitter_link'] ?? '' }}" target="_blank">Twitter</a></li>
                                <li><a href="{{ $social_media_link['linkedin_link'] ?? '' }}" target="_blank">LinkedIn</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer_copyright">
                            <div class="float-left">
                                <p>All Rights Reserved.</p>
                            </div>
                            <div class="float-right">
                                <p>© Copyright {{ date('Y') }}, <a href="https://www.amazing7.com/" target="_blank">Amazing7 Studios</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </footer>

<!-- Backtop Button -->
<div class="back-top"><a href="javascript:void(0)" style="display: inline-block;"><i class="fa fa-angle-up"></i></a></div>