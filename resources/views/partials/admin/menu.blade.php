<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin.home') }}" class="brand-link">
      <img src="{{ URL::to(config('constants.website_logo')) }}" alt="Red Trucking" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{ config('constants.website_name') }}</span>
    </a>
   

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
       <!--  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ isset(\Auth::user()->image) && !empty(\Auth::user()->image) && file_exists(\Auth::user()->image) ? URL::to(\Auth::user()->image) : URL::to(config('constants.no_user_image')) }}" class="img-circle elevation-2" alt="{{ isset(\Auth::user()->name) && !empty(\Auth::user()->name) ? \Auth::user()->name : '' }}">
        </div>
        <div class="info">
          <a href="#" class="d-block">  {{ isset(\Auth::user()->name) && !empty(\Auth::user()->name) ? \Auth::user()->name : '' }} </a>
        </div>
      </div> -->


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                

                <li class="nav-item">
                    <a href="{{ route('admin.home') }}" class="nav-link {{ request()->is('admin')? 'active' : '' }}">
                        
                            <i class="nav-icon fas fa-tachometer-alt">

                            </i>
                            <p><span>Dashboard </span>
                        </p>
                    </a>
                </li>

                

                <li class="nav-item">
                    <a href="{{ URL::to('admin/banner') }}" class="nav-link {{ request()->is('admin/banner')? 'active' : '' }}">
                        
                            <i class="nav-icon fa fa-window-restore">

                            </i>
                            <p><span>Home Banners </span>
                        </p>
                    </a>
                </li>

                 <li class="nav-item has-treeview {{ request()->is('admin/testimonial/add') || request()->is('admin/testimonial') || request()->is('admin/testimonial/*')  ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-align-center">

                            </i>
                            <p>
                                <span>Testimonial </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/testimonial/add') }}" class="nav-link {{ request()->is('admin/testimonial/add')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Add Testimonial</span>
                                        </p>
                                    </a>
                                </li>
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/testimonial') }}" class="nav-link {{ request()->is('admin/testimonial')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Testimonial Listing</span>
                                        </p>
                                    </a>
                                </li>


                        </ul>
                </li>


                <li class="nav-item">
                    <a href="{{ URL::to('admin/homepage') }}" class="nav-link {{ request()->is('admin/homepage') || request()->is('admin/homepage/addclient')? 'active' : '' }}">
                       
                            <i class="nav-icon fa fa-file">

                            </i>
                             <p><span>Home Page </span>
                        </p>
                    </a>
                </li>


                <li class="nav-item has-treeview {{ request()->is('admin/about') || request()->is('admin/about/safety') || request()->is('admin/about/sustainability') || request()->is('admin/about/addmember') ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-tags">

                            </i>
                            <p>
                                <span>About Us </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/about') }}" class="nav-link {{ request()->is('admin/about') || request()->is('admin/about/addmember') ? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>About</span>
                                        </p>
                                    </a>
                                </li>
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/about/safety') }}" class="nav-link {{ request()->is('admin/about/safety')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Safety</span>
                                        </p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/about/sustainability') }}" class="nav-link {{ request()->is('admin/about/sustainability')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Sustainability</span>
                                        </p>
                                    </a>
                                </li>

                        </ul>
                </li>

               

                <li class="nav-item has-treeview {{ request()->is('admin/services') || request()->is('admin/services/transport') || request()->is('admin/services/warehousing') || request()->is('admin/services/logistics') || request()->is('admin/services/transloading') ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-server">

                            </i>
                            <p>
                                <span>Services </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/services/transport') }}" class="nav-link {{ request()->is('admin/services/transport')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Transport</span>
                                        </p>
                                    </a>
                                </li>
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/services/warehousing') }}" class="nav-link {{ request()->is('admin/services/warehousing')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Warehousing</span>
                                        </p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/services/logistics') }}" class="nav-link {{ request()->is('admin/services/logistics')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Logistics</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/services/transloading') }}" class="nav-link {{ request()->is('admin/services/transloading')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Transloading</span>
                                        </p>
                                    </a>
                                </li>

                        </ul>
                </li>

                 <li class="nav-item">
                    <a href="{{ URL::to('admin/contactus') }}" class="nav-link {{ request()->is('admin/contactus')? 'active' : '' }}">
                        
                            <i class="nav-icon fa fa-address-book">

                            </i>
                           <p> <span>Contact Us </span>
                        </p>
                    </a>
                </li>


                <li class="nav-item">
                    <a href="{{ URL::to('admin/driver') }}" class="nav-link {{ request()->is('admin/driver')? 'active' : '' }}">
                        
                            <i class="nav-icon fa fa-truck">

                            </i>
                            <p><span>{{ config('constants.website_name') }} Driver </span>
                        </p>
                    </a>
                </li>

               <!--  <li class="nav-item">
                    <a href="{{ URL::to('admin/partner') }}" class="nav-link {{ request()->is('admin/partner')? 'active' : '' }}">
                        <p>
                            <i class="fa fa-address-card">

                            </i>
                            <span>{{ config('constants.website_name') }} Partner </span>
                        </p>
                    </a>
                </li> -->

                <li class="nav-item has-treeview {{ request()->is('admin/form/contact') || request()->is('admin/form/career')  ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-address-book">

                            </i>
                            <p>
                                <span>{{ config('constants.website_name') }} Forms </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/form/contact') }}" class="nav-link {{ request()->is('admin/form/contact')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Contact Forms</span>
                                        </p>
                                    </a>
                                </li>
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/form/career') }}" class="nav-link {{ request()->is('admin/form/career')? 'active' : '' }}">
                                        <i class=" nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Career Forms</span>
                                        </p>
                                    </a>
                                </li>


                        </ul>
                </li>

                <li class="nav-item has-treeview {{ request()->is('admin/blog/add') || request()->is('admin/blog') || request()->is('admin/blog/*')  ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-th-large">

                            </i>
                            <p>
                                <span>Blog </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/blog/add') }}" class="nav-link {{ request()->is('admin/blog/add')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Add Blog</span>
                                        </p>
                                    </a>
                                </li>
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/blog') }}" class="nav-link {{ request()->is('admin/blog')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Blog Listing</span>
                                        </p>
                                    </a>
                                </li>


                        </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ URL::to('admin/social') }}" class="nav-link {{ request()->is('admin/social')? 'active' : '' }}">
                        
                            <i class="nav-icon fa fa-share-square">

                            </i>
                           <p> <span>Social Media </span>
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ URL::to('admin/changepassword') }}" class="nav-link {{ request()->is('admin/changepassword')? 'active' : '' }}">
                        
                            <i class="nav-icon fa fa-unlock-alt">

                            </i>
                           <p> <span>Change Password </span>
                        </p>
                    </a>
                </li>

               
                <li class="nav-item">
                    <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        
                            <i class="nav-icon fas fa-sign-out-alt">

                            </i>
                           <p> <span>Logout</span>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>