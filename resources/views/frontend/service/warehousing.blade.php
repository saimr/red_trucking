@extends('layouts.frontend')
@section('content')

<!--Banner-->
    <section class="webpage_banner" style="background-image: url({{ URL::to(asset('images/frontend/webbanner/service-banner.jpg')) }})">
        <div class="row">
            <div class="col-md-12">
                <div class="webbanner-content">
                    <ul class="banner_breadcrumb">
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <li>Services</li>
                    </ul>
                    <h3 class="topbanner-head">WAREHOUSING</h3>
                </div>
            </div>
        </div>
    </section>
    @php
    $data = isset($warehousing_data->meta_data) && !empty($warehousing_data->meta_data) ? json_decode($warehousing_data->meta_data) : array() ;
    @endphp 

    @include('frontend.service.service_tab')

    <section class="service_listbox">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="service_listbox_content mb-5">
                        <h1 class="sub_heading wow fadeInDown">{{ $data->section_one_title ?? '' }}</h1>
                        <p class="wow fadeInUp">{{ $data->section_one_first_paragraphp ?? '' }}</p>

                        <p class="wow fadeInUp"> {{ $data->section_one_second_paragraphp ?? '' }}
                        </p>
                    </div>
                    <div class="service_listbox_content mb-5">
                        <h1 class="sub_heading wow fadeInDown">{{ $data->section_one_second_title ?? '' }}</h1>
                        <p class="wow fadeInUp">{{ $data->section_one_second_first_paragraphp ?? '' }}</p>

                        <p class="wow fadeInUp">{{ $data->section_one_second_second_paragraphp ?? '' }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="service_listbox_middle">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="service_listbox_midcontent">
                        <div class="service_listbox_content">
                            <h4 class="sub_heading wow fadeInDown">{{ $data->section_two_title ?? '' }}</h4>
                            <p class="wow fadeInUp">{{ $data->section_two_first_paragraphp ?? '' }}</p>

                            <p class="wow fadeInUp">{{ $data->section_two_second_paragraphp ?? '' }} </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="service_listbox mb-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 order-2 order-lg-1">
                    <div class="service_listbox_content mr-lg-5 mr-0">
                        <div class="service_listbox_content mb-5">
                            <h4 class="sub_heading wow fadeInDown">{{ $data->section_three_title ?? '' }}</h4>
                            <p class="wow fadeInUp">{{ $data->section_three_description ?? '' }}</p>
                        </div>

                        <div class="service_listbox_content  mb-5">
                            <h4 class="sub_heading wow fadeInDown">{{ $data->section_three_benefit_title ?? '' }}</h4>
                            @php
                            $benefits = isset($data->section_three_benefits) && !empty($data->section_three_benefits) ? json_decode($data->section_three_benefits) : array() ;
                            @endphp
                            <ul class="serlst_cout2">
                                @foreach($benefits as $p_key => $value)
                                <li class="wow fadeInUp">{{ $value ?? '' }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 d-flex align-items-center order-1 order-lg-2 mb-5 mb-lg-0">
                    <div class="logistic_img wow fadeInRight">
                        <img src="{{ URL::to($data->section_three_image) }}" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>

 @include('frontend.service.contact_us')


@endsection
@section('scripts')

@parent

@endsection