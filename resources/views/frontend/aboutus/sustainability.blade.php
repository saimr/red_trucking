@extends('layouts.frontend')
@section('content')

<!--Banner-->
    <section class="webpage_banner" style="background-image: url({{ URL::to(asset('images/frontend/webbanner/sustainability-banner.jpg')) }})">
        <div class="row">
            <div class="col-md-12">
                <div class="webbanner-content">
                    <ul class="banner_breadcrumb">
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <li>About Us</li>
                    </ul>
                    <h1 class="topbanner-head">Sustainability</h1>
                </div>
            </div>
        </div>
    </section>


     @php
    $sustainability_meta = isset($sustainability_data->meta_data) && !empty($sustainability_data->meta_data) ? json_decode($sustainability_data->meta_data) : array() ;
    @endphp 

    <section class="sustainability-top">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="sustabiltytop-img wow fadeInLeft">
                        <img src="{{  isset($sustainability_meta->first_image) && file_exists($sustainability_meta->first_image) ? URL::to($sustainability_meta->first_image):''  }}" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="sustabilty-content mt-lg-5 mt-md-0 mt-4 mr-lg-4 mr-0 wow fadeInUp">
                        <p class="mb-0">{{ isset($sustainability_meta->first_content) ? $sustainability_meta->first_content : '' }} </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sustainability-right">
        <div class="container">
            <div class="row">
                <div class="col-md-6 order-2 order-md-1">
                    <div class="sustabiltyrgt-content mt-2 mt-md-5 mr-md-4 mr-0 wow fadeInUp">
                        <p>{{ isset($sustainability_meta->second_content) ? $sustainability_meta->second_content : '' }} </p>
                    </div>
                </div>
                <div class="col-md-6  order-1 order-md-2 ">
                    <div class="sustabiltyrgt-img wow fadeInLeft">
                        <img src="{{  isset($sustainability_meta->second_image) && file_exists($sustainability_meta->second_image) ? URL::to($sustainability_meta->second_image):''  }}" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sustainability-middle">
        <h3 class="sustainablity-faded wow slideInUp">Sustainability</h3>
        <div class="sustainability-background"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="sustabiltymid-content mt-lg-5 mt-md-4 mr-lg-4">
                        <p class="wow fadeInUp text-white">{{ isset($sustainability_meta->third_content) ? $sustainability_meta->third_content : '' }} </p>
                    </div>
                </div>
                <div class="col-md-6 offset-md-1">
                    <div class="sustabiltymdl-img wow slideInRight">
                        <img src="{{  isset($sustainability_meta->third_image) && file_exists($sustainability_meta->third_image) ? URL::to($sustainability_meta->third_image):''  }}" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sustainability-bottom py-4 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sustabiltybtm-content">
                        <p class="mb-0 wow fadeInUp">{{ isset($sustainability_meta->bottom_content) ? $sustainability_meta->bottom_content : '' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
@section('scripts')
@parent

@endsection