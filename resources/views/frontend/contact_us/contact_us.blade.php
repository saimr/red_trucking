@extends('layouts.frontend')
@section('content')

<!--Banner-->
    <section class="webpage_banner" style="background-image: url({{ URL::to(asset('images/frontend/webbanner/about-banner.jpg')) }})">
        <div class="row">
            <div class="col-md-12">
                <div class="webbanner-content">
                    <ul class="banner_breadcrumb">
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <li>Contact</li>
                    </ul>
                    <h3 class="topbanner-head">Contact Us</h3>
                </div>
            </div>
        </div>
    </section>

    @php
    $contact_us_meta = isset($contact_us_data->meta_data) && !empty($contact_us_data->meta_data) ? json_decode($contact_us_data->meta_data) : array() ;
    @endphp 


    <!--Get in touch Contact-->
    <section class="contact-getintouch">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="getIntouch-content">
                        <h1 class="left-title wow fadeInDown">Get in touch</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-gitcont">
            <div class="faded-background-getintouch"></div>
            <div class="container">
                <div class="getIntouch-content">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="getIntouch-contlist mb-4 mb-md-0">
                                <div class="contlistimg wow flipInX">
                                    <img src="{{ URL::to(asset('images/frontend/svg/hotel-with-three-floors.svg')) }}">
                                </div>
                                <h4 class="mb-3 sub_heading wow fadeInDown">RED HQ</h4>
                                <p class="wow fadeInUp"><a href="{{  isset($contact_us_meta->red_headquarter_email_google_map_link) ? $contact_us_meta->red_headquarter_email_google_map_link : 'javscript:'}}">{{ isset($contact_us_meta->red_headquarter_address) ? $contact_us_meta->red_headquarter_address : '' }} <br>{{  isset($contact_us_meta->red_headquarter_address1) ? $contact_us_meta->red_headquarter_address1 : '' }}</a></p>
                                <div class="git-addemil wow fadeInUp">
                                    <p>OFFICE: <a href="tel:{{ isset($contact_us_meta->red_headquarter_phone) ? str_replace('-','',$contact_us_meta->red_headquarter_phone ): '' }}">{{ isset($contact_us_meta->red_headquarter_phone) ? $contact_us_meta->red_headquarter_phone : '' }}</a></p>
                                    <p><a href="mailto:{{ isset($contact_us_meta->red_headquarter_email) ? $contact_us_meta->red_headquarter_email : '' }}">{{ isset($contact_us_meta->red_headquarter_email) ? $contact_us_meta->red_headquarter_email : '' }}</a></p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="getIntouch-contlist">
                                <div class="contlistimg wow flipInX">
                                    <img src="{{ URL::to(asset('images/frontend/svg/shipping.svg')) }}">
                                </div>
                                <h4 class="mb-3 sub_heading wow fadeInDown">Warehouse</h4>
                                <p class="wow fadeInUp"><a href="{{ isset($contact_us_meta->warehouse_google_map_link) ? $contact_us_meta->warehouse_google_map_link : 'javscript:' }}">{{ isset($contact_us_meta->warehouse_address) ? $contact_us_meta->warehouse_address : '' }} <br>{{ isset($contact_us_meta->warehouse_address1) ? $contact_us_meta->warehouse_address1 : '' }}</a></p>
                                <div class="git-addemil wow fadeInUp">
                                    <p>OFFICE: <a href="tel:{{ isset($contact_us_meta->warehouse_phone) ? str_replace('-','',$contact_us_meta->warehouse_phone) : '' }}">{{ isset($contact_us_meta->warehouse_phone) ? $contact_us_meta->warehouse_phone : ''}}</a></p>
                                    <p><a href="mailto:{{ isset($contact_us_meta->warehouse_email) ? $contact_us_meta->warehouse_email : '' }}">{{ isset($contact_us_meta->warehouse_email) ? $contact_us_meta->warehouse_email : '' }}</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="getIntouch-contlist">
                                <div class="contlistimg wow flipInX">
                                    <img src="{{ URL::to(asset('images/frontend/svg/headphones.svg')) }}">
                                </div>
                                <h4 class="mb-3 sub_heading wow fadeInDown">Sales</h4>
                                <p class="wow fadeInUp"><a href="{{  isset($contact_us_meta->sales_google_map_link) ? $contact_us_meta->sales_google_map_link : 'javscript:'}}">{{ isset($contact_us_meta->sales_address) ? $contact_us_meta->sales_address : '' }} <br>{{  isset($contact_us_meta->sales_address1) ? $contact_us_meta->sales_address1 : '' }}</a></p>
                                <div class="git-addemil wow fadeInUp">
                                    <p>OFFICE: <a href="tel:{{ isset($contact_us_meta->sales_phone) ? str_replace('-','',$contact_us_meta->sales_phone) : '' }}">{{ isset($contact_us_meta->sales_phone) ? $contact_us_meta->sales_phone : '' }}</a></p>
                                    <p><a href="mailto:{{ isset($contact_us_meta->sales_email) ? $contact_us_meta->sales_email : '' }}">{{ isset($contact_us_meta->sales_email) ? $contact_us_meta->sales_email : '' }}</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="getIntouch-contlist">
                                <div class="contlistimg wow flipInX">
                                    <img src="{{ URL::to(asset('images/frontend/svg/recruitment.svg')) }}">
                                </div>
                                <h4 class="mb-3 sub_heading wow fadeInDown">Recruiting</h4>
                                <p class="wow fadeInUp"><a href="{{  isset($contact_us_meta->recruiting_google_map_link) ? $contact_us_meta->recruiting_google_map_link : 'javscript:'}}">{{ isset($contact_us_meta->recruiting_address) ? $contact_us_meta->recruiting_address : '' }} <br>{{  isset($contact_us_meta->recruiting_address1) ? $contact_us_meta->recruiting_address1 : '' }}</a></p>
                                <div class="git-addemil wow fadeInUp">
                                    <p>OFFICE: <a href="tel:{{ isset($contact_us_meta->recruiting_phone) ? str_replace('-','',$contact_us_meta->recruiting_phone) : '' }}">{{ isset($contact_us_meta->recruiting_phone) ? $contact_us_meta->recruiting_phone : '' }}</a></p>
                                    <p><a href="mailto:{{ isset($contact_us_meta->recruiting_email) ? $contact_us_meta->recruiting_email : '' }}">{{ isset($contact_us_meta->recruiting_email) ? $contact_us_meta->recruiting_email : '' }}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

      @include('frontend.service.contact_us')
    <section class="cont-map">
        @if( isset($contact_us_meta->google_embed_map) && !empty($contact_us_meta->google_embed_map) )
        @php 
        echo $contact_us_meta->google_embed_map;
        @endphp
        @endif
        
    </section>


@endsection
@section('scripts')
@parent

@endsection
