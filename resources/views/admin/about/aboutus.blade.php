@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>About Us</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/about/updateabout') }}" method="POST" enctype="multipart/form-data">
            @csrf
           
            @php
            $owner_meta = isset($about_us_owner_data->meta_data) && !empty($about_us_owner_data->meta_data) ? json_decode($about_us_owner_data->meta_data) : array() ;
            @endphp 

            <div class="form-group {{ $errors->has('owner_message') ? 'has-error' : '' }}">
                <label for="description">Owner Message *</label>
                <textarea rows="10"  id="owner_message" name="owner_message" class="form-control ">{{ old('owner_message', isset($owner_meta->owner_message) ? $owner_meta->owner_message : '') }}</textarea>
                @if($errors->has('owner_message'))
                    <p class="help-block">
                        {{ $errors->first('owner_message') }}
                    </p>
                @endif
                
            </div>
            
            <div class="form-group {{ $errors->has('images') ? 'has-error' : '' }}">
                <label for="description">Signature*</label> 

                @if(isset($owner_meta->signature) && file_exists($owner_meta->signature))
                
                <img src="{{ URL::to($owner_meta->signature) }}" height="100" width="100">
                <input type="hidden" name="old_signature_image" value="{{ $owner_meta->signature }}">
                @endif


                <br/>
                <p>Note : Image dimension should be within min 100X30 and max 300X92 pixels.</p>
                <input type="file" name="signature" accept="image/*">
                @if($errors->has('signature'))
                    <p class="help-block">
                        {{ $errors->first('signature') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('designation') ? 'has-error' : '' }}">
                <label for="designation">Designation*</label>
                <input type="text" id="designation" name="designation" class="form-control" value="{{ old('designation', isset($owner_meta->designation) ? $owner_meta->designation : '') }}">
                @if($errors->has('designation'))
                    <p class="help-block">
                        {{ $errors->first('designation') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="meta_id" value="{{ isset($about_us_owner_data->id) ? $about_us_owner_data->id : '0' }}">


             <hr/>

            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($data->title) ? $data->title : '') }}">
                @if($errors->has('title'))
                    <p class="help-block">
                        {{ $errors->first('title') }}
                    </p>
                @endif
                
            </div>
           

             <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Description *</label>
                <textarea rows="10"  id="description" name="description" class="form-control ">{{ old('description', isset($data) ? $data->description : '') }}</textarea>
                @if($errors->has('description'))
                    <p class="help-block">
                        {{ $errors->first('description') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('images') ? 'has-error' : '' }}">
                <label for="description">Left  Image</label> 

                @if(isset($left_image_data->name) && file_exists($left_image_data->name))
                
                <img src="{{ URL::to($left_image_data->name) }}" height="100" width="100">
                @endif

                 @if(isset($left_image_data->id))
                   <input type="hidden" name="gallery_id" value="{{ $left_image_data->id }}">
                   <input type="hidden" name="gallery_image_name" value="{{ $left_image_data->name }}">
                @endif

                <br/>
                <p>Note : Image dimension should be within min 840X452 pixels.</p>
                <input type="file" name="images" accept="image/*">
                @if($errors->has('images'))
                    <p class="help-block">
                        {{ $errors->first('images') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Right  Image</label> 

                @if(isset($right_image_data->name) && file_exists($right_image_data->name))
                
                <img src="{{ URL::to($right_image_data->name) }}" height="100" width="100">
                @endif

                 @if(isset($right_image_data->id))
                   <input type="hidden" name="right_gallery_id" value="{{ $right_image_data->id }}">
                   <input type="hidden" name="right_gallery_image_name" value="{{ $right_image_data->name }}">
                @endif

                <br/>
                <p>Note : Image dimension should be within min 840X452 pixels.</p>
                <input type="file" name="right_images" accept="image/*">
                @if($errors->has('images'))
                    <p class="help-block">
                        {{ $errors->first('right_images') }}
                    </p>
                @endif
                
            </div>
             <hr/>

             <div class="form-group {{ $errors->has('our_mission') ? 'has-error' : '' }}">
                <label for="our_mission"> Our Mission  *</label>
                <textarea rows="5"  id="our_mission" name="our_mission" class="form-control tinymce_editor ">{{ old('sort_description', isset($data->sort_description) ? $data->sort_description : '') }}</textarea>
                @if($errors->has('our_mission'))
                    <p class="help-block">
                        {{ $errors->first('our_mission') }}
                    </p>
                @endif
                
            </div>

            <hr/>
             @php
            $safety_meta = isset($safety_data->meta_data) && !empty($about_us_owner_data->meta_data) ? json_decode($safety_data->meta_data) : array() ;
            @endphp 

             <label for="safety_content"> SAFETY  *</label>

             <div class="form-group {{ $errors->has('safety_content') ? 'has-error' : '' }}">
                <label for="safety_content"> Sort Content*</label>
                <textarea rows="5"  id="safety_content" name="safety_content" class="form-control ">{{ old('safety_content', isset($safety_meta->content) ? $safety_meta->content : '') }}</textarea>
                @if($errors->has('safety_content'))
                    <p class="help-block">
                        {{ $errors->first('safety_content') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('safety_images') ? 'has-error' : '' }}">
                <label for="description">Upload Image*</label> 

                @if(isset($safety_meta->safety_images) && file_exists($safety_meta->safety_images))
                
                <img src="{{ URL::to($safety_meta->safety_images) }}" height="100" width="100">
                <input type="hidden" name="old_safety_images" value="{{ $safety_meta->safety_images }}">
                @endif


                <br/>
                <p>Note : Image dimension should be within min 855X444 pixels.</p>
                <input type="file" name="safety_images" accept="image/*">
                @if($errors->has('safety_images'))
                    <p class="help-block">
                        {{ $errors->first('safety_images') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="safety_id" value="{{ $safety_data->id?? ''}}">

            <hr/>
            @php
            $sustainability_meta = isset($sustainability_data->meta_data) && !empty($sustainability_data->meta_data) ? json_decode($sustainability_data->meta_data) : array() ;
            @endphp 
             <label for="our_mission"> SUSTAINABILITY  *</label>
             <div class="form-group {{ $errors->has('sustainability_content') ? 'has-error' : '' }}">
                <label for="our_mission"> Sort Content*</label>
                <textarea rows="5"  id="sustainability_content" name="sustainability_content" class="form-control ">{{ old('sort_description', isset($sustainability_meta->content) ? $sustainability_meta->content : '') }}</textarea>
                @if($errors->has('sustainability_content'))
                    <p class="help-block">
                        {{ $errors->first('sustainability_content') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('sustainability_images') ? 'has-error' : '' }}">
                <label for="description">Upload Image*</label> 

                @if(isset($sustainability_meta->sustainability_images) && file_exists($sustainability_meta->sustainability_images))
                
                <img src="{{ URL::to($sustainability_meta->sustainability_images) }}" height="100" width="100">
                <input type="hidden" name="old_sustainability_images" value="{{ $sustainability_meta->sustainability_images }}">
                @endif


                <br/>
                <p>Note : Image dimension should be within min 855X444 pixels.</p>
                <input type="file" name="sustainability_images" accept="image/*">
                @if($errors->has('sustainability_images'))
                    <p class="help-block">
                        {{ $errors->first('sustainability_images') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="sustainability_id" value="{{ $sustainability_data->id?? ''}}">

            <hr/>

             @php
            $red_family__meta = isset($red_family_data->meta_data) && !empty($red_family_data->meta_data) ? json_decode($red_family_data->meta_data) : array() ;
            @endphp 

            <div class="form-group {{ $errors->has('red_family_content') ? 'has-error' : '' }}">
                <label for="red_family_content">RED FAMILY  Sort Content *</label>
                <textarea rows="1"  id="red_family_content" name="red_family_content" class="form-control ">{{ old('red_family_content', isset($red_family__meta->content) ? $red_family__meta->content : '') }}</textarea>
                @if($errors->has('red_family_content'))
                    <p class="help-block">
                        {{ $errors->first('red_family_content') }}
                    </p>
                @endif
                <input type="hidden" name="red_family_content_id" value="{{ isset($red_family_data->id) ? $red_family_data->id : '0' }}">
                
            </div>

            <div align="center"> <h1>RED FAMILY MEMBERS</h1></div>
            <br/>

            <a href="{{ URL::to('admin/about/addmember') }}" class="btn btn-info">Add New Red Family Member</a>
            <br/>
            <br/>
            @if(!empty($all_members) && sizeof($all_members))

            <div class="row">
                @foreach($all_members as $key => $value)
                <div class="col-md-4 border" style="width: 18rem; padding: 10px;">
                  <img class="card-img-top" src="{{ URL::to($value->image) }}" alt="{{ $value->name}}">
                  <div class="card-body">
                    <h5 class="card-title">{{ $value->name}}</h5>
                    <p class="card-text">{{ $value->designation}} </p>
                    <a href="{{ URL::to('admin/about/editmember/'.$value->id) }}" class="btn btn-primary">Edit</a>
                  </div>
                </div>
                @endforeach
            </div>
             @else
                   <h3> No Record found !</h3>
             @endif



            
            <div class="mt-3">
                <input type="hidden" name="id" value="{{ isset($data->id) ? $data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection