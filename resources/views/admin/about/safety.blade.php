@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Safety </b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/about/updatesafety') }}" method="POST" enctype="multipart/form-data">
            @csrf

             @php
            $data = isset($safety_data->meta_data) && !empty($safety_data->meta_data) ? json_decode($safety_data->meta_data) : array() ;
            @endphp 

            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($data->title) ? $data->title : '') }}">
                @if($errors->has('title'))
                    <p class="help-block">
                        {{ $errors->first('title') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Left Content *</label>
                <textarea rows="5"  id="left_content" name="left_content" class="form-control ">{{ old('left_content', isset($data->left_content) ? $data->left_content : '') }}</textarea>
                @if($errors->has('left_content'))
                    <p class="help-block">
                        {{ $errors->first('left_content') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('left_image') ? 'has-error' : '' }}">
                <label for="description">Left Image</label> 

                @if(isset($data->left_image) && file_exists($data->left_image))
                
                <img src="{{ URL::to($data->left_image) }}" height="100" width="100">
                 <input type="hidden" name="old_left_image" value="{{ $data->left_image }}">
                @endif

                

                <br/>
                <p>Note : Image dimension should be within min 340X290 pixels.</p>
                <input type="file" name="left_image" accept="image/*">
                @if($errors->has('left_image'))
                    <p class="help-block">
                        {{ $errors->first('left_image') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('middle_content') ? 'has-error' : '' }}">
                <label for="description">Middle Content *</label>
                <textarea rows="10"  id="description" name="middle_content" class="form-control ">{{ old('middle_content', isset($data->middle_content) ? $data->middle_content : '') }}</textarea>
                @if($errors->has('middle_content'))
                    <p class="help-block">
                        {{ $errors->first('middle_content') }}
                    </p>
                @endif
                
            </div>



             <div class="form-group {{ $errors->has('right_content') ? 'has-error' : '' }}">
                <label for="description">Right Content *</label>
                <textarea rows="10"  id="right_content" name="right_content" class="form-control ">{{ old('description', isset($data->right_content) ? $data->right_content : '') }}</textarea>
                @if($errors->has('right_content'))
                    <p class="help-block">
                        {{ $errors->first('right_content') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('right_image') ? 'has-error' : '' }}">
                <label for="description">Right Image</label> 

                @if(isset($data->right_image) && file_exists($data->right_image))
                
                <img src="{{ URL::to($data->right_image) }}" height="100" width="100">
                <input type="hidden" name="old_right_image" value="{{ $data->right_image }}">
                @endif


                <br/>
                <p>Note : Image dimension should be within min 340X290 pixels.</p>
                <input type="file" name="right_image" accept="image/*">
                @if($errors->has('right_image'))
                    <p class="help-block">
                        {{ $errors->first('right_image') }}
                    </p>
                @endif
                
            </div>

            
            <div>
                <input class="btn btn-danger" type="hidden" name="id" value="{{ isset($safety_data->id) ? $safety_data->id : '0'}}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection