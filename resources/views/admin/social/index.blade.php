@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Social Media</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/social/update') }}" method="POST" enctype="multipart/form-data">
            @csrf
           
            @php
            $contact_us_meta = isset($social_media_data->meta_data) && !empty($social_media_data->meta_data) ? json_decode($social_media_data->meta_data) : array() ;
            @endphp 

           

            <div class="form-group {{ $errors->has('facebook_link') ? 'has-error' : '' }}">
                <label for="designation">Facebook*</label>
                <input type="text" id="facebook_link" name="facebook_link" class="form-control" placeholder="https://www.facebook.com/" value="{{ old('facebook_link', isset($contact_us_meta->facebook_link) ? $contact_us_meta->facebook_link : '') }}">
                @if($errors->has('facebook_link'))
                    <p class="help-block">
                        {{ $errors->first('facebook_link') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('twitter_link') ? 'has-error' : '' }}">
                <label for="designation">Twitter*</label>
                <input type="text" id="twitter_link" name="twitter_link" class="form-control" placeholder="https://www.instagram.com/?hl=en" value="{{ old('twitter_link', isset($contact_us_meta->twitter_link) ? $contact_us_meta->twitter_link : '') }}">
                @if($errors->has('twitter_link'))
                    <p class="help-block">
                        {{ $errors->first('twitter_link') }}
                    </p>
                @endif
                
            </div>

             <div class="form-group {{ $errors->has('linkedin_link') ? 'has-error' : '' }}">
                <label for="designation">Linkedin*</label>
                <input type="text" id="linkedin_link" name="linkedin_link" class="form-control" placeholder="https://in.linkedin.com/" value="{{ old('linkedin_link', isset($contact_us_meta->linkedin_link) ? $contact_us_meta->linkedin_link : '') }}">
                @if($errors->has('linkedin_link'))
                    <p class="help-block">
                        {{ $errors->first('linkedin_link') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('instagram_link') ? 'has-error' : '' }}">
                <label for="designation">Instagram*</label>
                <input type="text" id="instagram_link" name="instagram_link" class="form-control" placeholder="https://in.linkedin.com/" value="{{ old('instagram_link', isset($contact_us_meta->instagram_link) ? $contact_us_meta->instagram_link : '') }}">
                @if($errors->has('instagram_link'))
                    <p class="help-block">
                        {{ $errors->first('instagram_link') }}
                    </p>
                @endif
                
            </div>
            

            
            
            
            <div>
                <input type="hidden" name="id" value="{{ isset($social_media_data->id) ? $social_media_data->id:'0' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection