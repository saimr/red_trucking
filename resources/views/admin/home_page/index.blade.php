@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Home Page</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/homepage/update') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <label> About Us </label>
            @php
            $about_us_meta = isset($about_us_meta_data->meta_data) && !empty($about_us_meta_data->meta_data) ? json_decode($about_us_meta_data->meta_data) : array() ;
            @endphp 
         
             <div class="form-group {{ $errors->has('about_us_content') ? 'has-error' : '' }}">
                <label for="about_us_content">Sort Content *</label>
                <textarea rows="10"  id="about_us_content" name="about_us_content" class="form-control ">{{ old('about_us_content', isset($about_us_meta) && !empty($about_us_meta->content) ? $about_us_meta->content : '') }}</textarea>
                @if($errors->has('about_us_content'))
                    <p class="help-block">
                        {{ $errors->first('about_us_content') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="about_us_title" value="About Us" >
            <input type="hidden" name="about_us_id" value="{{ isset($about_us_meta_data) ? $about_us_meta_data->id : '' }}" >

            

            <div class="form-group {{ $errors->has('about_us_upload_video') ? 'has-error' : '' }}">
                <label for="about_us_upload_video">Upload Video</label> 

                @if(isset($about_us_meta->video_url) && file_exists($about_us_meta->video_url))
                <br/>
                <input type="hidden" name="old_about_us_upload_video" value="{{ $about_us_meta->video_url }}">
                <video width="150" height="100" controls>
                  <source src="{{ URL::to($about_us_meta->video_url) }}" type="video/mp4">
                  <source src="{{ URL::to($about_us_meta->video_url) }}" type="video/ogg">
                Your browser does not support the video tag.
                </video>
                <br/>
                @endif


                <input type="file" name="about_us_upload_video" >
                @if($errors->has('about_us_upload_video'))
                    <p class="help-block">
                        {{ $errors->first('about_us_upload_video') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('about_us_video_image') ? 'has-error' : '' }}">
                <label for="about_us_video_image">Video Poster Image</label> 

                @if(isset($about_us_meta->video_poster_image) && file_exists($about_us_meta->video_poster_image))
                <input type="hidden" name="old_about_us_video_image" value="{{ $about_us_meta->video_poster_image }}">
                <img src="{{ URL::to($about_us_meta->video_poster_image) }}" height="100" width="100"> <br/>
                @endif

                <input type="file" name="about_us_video_image" accept="image/*">
                @if($errors->has('about_us_video_image'))
                    <p class="help-block">
                        {{ $errors->first('about_us_video_image') }}
                    </p>
                @endif
                
            </div>
            <hr />
            <label> TRUST </label>
            @php
            $trust_meta = isset($trust_meta_data->meta_data) && !empty($trust_meta_data->meta_data) ? json_decode($trust_meta_data->meta_data) : array() ; 
            @endphp 

            <div class="form-group {{ $errors->has('trust_content') ? 'has-error' : '' }}">
                <label for="trust_content">Sort Content *</label>
                <textarea rows="10"  id="trust_content" name="trust_content" class="form-control ">{{ old('trust_content', isset($trust_meta) && !empty($trust_meta->content) ? $trust_meta->content : '') }}</textarea>
                @if($errors->has('trust_content'))
                    <p class="help-block">
                        {{ $errors->first('trust_content') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="trust_title" value="Trust" >
            <input type="hidden" name="trust_id" value="{{ isset($trust_meta_data) ? $trust_meta_data->id : '' }}" >

            <hr />
            <label> SECURITY </label>
            @php
            $security_meta = isset($security_meta_data->meta_data) && !empty($security_meta_data->meta_data) ? json_decode($security_meta_data->meta_data) : array() ; 
            @endphp 

            <div class="form-group {{ $errors->has('security_content') ? 'has-error' : '' }}">
                <label for="security_content">Sort Content *</label>
                <textarea rows="10"  id="security_content" name="security_content" class="form-control ">{{ old('security_content', isset($security_meta) && !empty($security_meta->content) ? $security_meta->content : '') }}</textarea>
                @if($errors->has('security_content'))
                    <p class="help-block">
                        {{ $errors->first('security_content') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="security_title" value="Security" >
            <input type="hidden" name="security_id" value="{{ isset($security_meta_data) ? $security_meta_data->id : '' }}" >

            <hr />
            <label> SUSTAINABILITY </label>
            @php
            $sustainability_meta = isset($sustainability_meta_data->meta_data) && !empty($sustainability_meta_data->meta_data) ? json_decode($sustainability_meta_data->meta_data) : array() ; 
            @endphp 

            <div class="form-group {{ $errors->has('sustainability_content') ? 'has-error' : '' }}">
                <label for="sustainability_content">Sort Content *</label>
                <textarea rows="10"  id="sustainability_content" name="sustainability_content" class="form-control ">{{ old('sustainability_content', isset($sustainability_meta) && !empty($sustainability_meta->content) ? $sustainability_meta->content : '') }}</textarea>
                @if($errors->has('sustainability_content'))
                    <p class="help-block">
                        {{ $errors->first('sustainability_content') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="sustainability_title" value="Sustainability" >
            <input type="hidden" name="sustainability_id" value="{{ isset($sustainability_meta_data) ? $sustainability_meta_data->id : '' }}" >

            <hr />

            <label>  SERVICE AREAS </label>
            @php
            $service_areas_meta = isset($service_areas_meta_data->meta_data) && !empty($service_areas_meta_data->meta_data) ? json_decode($service_areas_meta_data->meta_data) : array() ; 
            @endphp 

            <div class="form-group {{ $errors->has('service_areas_content') ? 'has-error' : '' }}">
                <label for="service_areas_content">Sort Content *</label>
                <textarea rows="10"  id="service_areas_content" name="service_areas_content" class="form-control ">{{ old('service_areas_content', isset($service_areas_meta) && !empty($service_areas_meta->content) ? $service_areas_meta->content : '') }}</textarea>
                @if($errors->has('service_areas_content'))
                    <p class="help-block">
                        {{ $errors->first('service_areas_content') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="service_areas_title" value="Service Areas" >
            <input type="hidden" name="service_areas_id" value="{{ isset($service_areas_meta_data) ? $service_areas_meta_data->id : '' }}" >

            
            <hr />
            <label>  RED SERVICES </label>
            @php
            $red_service_meta = isset($red_service_meta_data->meta_data) && !empty($red_service_meta_data->meta_data) ? json_decode($red_service_meta_data->meta_data) : array() ; 
            @endphp 

            <div class="form-group {{ $errors->has('red_service_content') ? 'has-error' : '' }}">
                <label for="red_service_content">Sort Content *</label>
                <textarea rows="10"  id="red_service_content" name="red_service_content" class="form-control ">{{ old('red_service_content', isset($red_service_meta) && !empty($red_service_meta->content) ? $red_service_meta->content : '') }}</textarea>
                @if($errors->has('red_service_content'))
                    <p class="help-block">
                        {{ $errors->first('red_service_content') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="red_service_title" value="Red Services" >
            <input type="hidden" name="red_service_id" value="{{ isset($red_service_meta_data) ? $red_service_meta_data->id : '' }}" >

            @php 
            /*
            @endphp

            <hr />
            <label>  PARTNER</label>
            @php
            $partner_meta = isset($partner_meta_data->meta_data) && !empty($partner_meta_data->meta_data) ? json_decode($partner_meta_data->meta_data) : array() ; 
            @endphp 

            <div class="form-group {{ $errors->has('partner_content') ? 'has-error' : '' }}">
                <label for="partner_content">Sort Content *</label>
                <textarea rows="10"  id="partner_content" name="partner_content" class="form-control ">{{ old('partner_content', isset($partner_meta) && !empty($partner_meta->content) ? $red_service_meta->content : '') }}</textarea>
                @if($errors->has('partner_content'))
                    <p class="help-block">
                        {{ $errors->first('partner_content') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="partner_title" value="Partner" >
            <input type="hidden" name="partner_id" value="{{ isset($partner_meta_data) ? $partner_meta_data->id : '' }}" >

             @php 
            */
            @endphp

 
            <hr />
            <label>  RESOURCES</label>
            @php
            $resource_meta = isset($resource_meta_data->meta_data) && !empty($resource_meta_data->meta_data) ? json_decode($resource_meta_data->meta_data) : array() ; 
            @endphp 

            <div class="form-group {{ $errors->has('resource_content') ? 'has-error' : '' }}">
                <label for="resource_content">Sort Content *</label>
                <textarea rows="10"  id="resource_content" name="resource_content" class="form-control ">{{ old('resource_content', isset($resource_meta) && !empty($resource_meta->content) ? $resource_meta->content : '') }}</textarea>
                @if($errors->has('resource_content'))
                    <p class="help-block">
                        {{ $errors->first('resource_content') }}
                    </p>
                @endif
                
            </div>
            <input type="hidden" name="resource_title" value="Resource" >
            <input type="hidden" name="resource_id" value="{{ isset($resource_meta_data) ? $resource_meta_data->id : '' }}" >

             <hr/>

             <div align="center"> <h1>HAPPY CLIENTS</h1></div>
            <br/>

             <a href="{{ URL::to('admin/homepage/addclient') }}" class="btn btn-info">Add New Happy Client</a>
            <br/>
            <br/>

            @if(!empty($all_clients) && sizeof($all_clients))

            <div class="row">
                @foreach($all_clients as $key => $value)
                <div class="col-md-4 border" style="padding: 10px;">
                  <img class="card-img-top" src="{{ URL::to($value->image) }}" alt="{{ $value->name}}">
                  <div class="card-body">
                    <h5 class="card-title">{{ $value->name}}</h5>
                    <p class="card-text">{{ $value->url}} </p>
                    <a href="{{ URL::to('admin/homepage/editclient/'.$value->id) }}" class="btn btn-primary">Edit</a>
                  </div>
                </div>
                @endforeach

                
            </div>

            <div class="card-footer clearfix">
                 {!! $all_clients->links() !!}
            </div>

             @else
                   <h3 align="center"> No Record found !</h3>
             @endif


             



            <div>
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection