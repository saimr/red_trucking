@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Testimonial Listing</b></h4>
    </div>
        <div class="card-body table-responsive p-0">
           @if(!empty($all_testimonials) && sizeof($all_testimonials))
                <table class="table table-hover">
                  <thead>
                    <tr>
                      
                      <th>Image</th>
                      <th>Name</th>
                      <th>Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($all_testimonials as $key => $value)
                    <tr>
                      <td>
                        @if( $value->image && !empty($value->image) && file_exists($value->image) )
                        <img src="{{ URL::to($value->image) }}" height="50" width="50">
                         @endif
                       </td>
                       <td>{{ $value->name ?? '' }} </td>
                     
                      <td>{{ $value->created_at ? date('d-M-Y h:i:s',strtotime($value->created_at)) : '' }}</td>
                      <td>
                        <div class="btn-group btn-group-sm">
                          <a href="{{ URL::to('admin/testimonial/edit/'.$value->id) }}" class="btn btn-info" title="Edit"><i class="fas fa-edit"></i></a>
                          &nbsp;
                          <a href="javascript:" class="btn btn-danger blog_delete" title="Delete" delete_blog_url="{{ URL::to('admin/testimonial/delete/'.$value->id) }}"><i class="fas fa-trash"></i></a>
                        </div>
                    </td>
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
                @else
                    <tr align="center">
                      <td colspan="6" align="center">No Record found !</td>                      
                    </tr>
                    @endif
        </div>

        <div class="card-footer clearfix">
                 {!! $all_testimonials->links() !!}
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection