@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Transport</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/services/updatetransport') }}" class="validate_form" method="POST" enctype="multipart/form-data">
            @csrf
            @php
            $data = isset($transport_data->meta_data) && !empty($transport_data->meta_data) ? json_decode($transport_data->meta_data) : array() ;
            @endphp 
            <hr/>
            <h2>Section 1</h2>
            <hr/>

            <div class="form-group {{ $errors->has('section_one_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_one_title" name="section_one_title" class="form-control validate[required]" value="{{ old('section_one_title', isset($data->section_one_title) ? $data->section_one_title : '') }}">
                @if($errors->has('section_one_title'))
                    <p class="help-block">
                        {{ $errors->first('section_one_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_description') ? 'has-error' : '' }}">
                <label for="section_one_description">Description *</label>
                <textarea id="section_one_description" name="section_one_description" class="form-control validate[required] ">{{ old('section_one_description', isset($data->section_one_description) ? $data->section_one_description : '') }}</textarea>
                @if($errors->has('section_one_description'))
                    <p class="help-block">
                        {{ $errors->first('section_one_description') }}
                    </p>
                @endif
                
            </div>

            <label for="name">Transport Service</label>
            <div class="form-group {{ $errors->has('section_one_service_title') ? 'has-error' : '' }}">
                
                <label for="name"><br/> Title*</label>
                <input type="text" id="section_one_service_title" name="section_one_service_title" class="form-control validate[required]" value="{{ old('section_one_service_title', isset($data->section_one_service_title) ? $data->section_one_service_title : '') }}">
                @if($errors->has('section_one_service_title'))
                    <p class="help-block">
                        {{ $errors->first('section_one_service_title') }}
                    </p>
                @endif
                
            </div>

            @php
            $services = isset($data->section_one_services) && !empty($data->section_one_services) ? json_decode($data->section_one_services) : array() ;
            @endphp
            
            @if(is_array($services) && sizeof($services) == 0 )
            <div class="form-group service_remove_div">
                <label for="name"> Service*</label>
                <input type="text"  name="section_one_services[]" class=" form-control validate[required]" value="">
            </div>
            @endif

            <div id="add_more_services_section_one_result">
                 @foreach($services as $key => $value)
                <div class="form-group service_remove_div remove_div{{$key+1}}" >
                    <label for="name"> Service*</label>
                    <button type="button" class="btn btn-danger   add_more_remove float-right" counter="{{$key+1}}"> Remove</button>
                    <input type="text"  name="section_one_services[]" class=" validate[required] form-control" value="{{ $value ?? '' }}">
                </div>
                @endforeach
            </div>
            @if($errors->has('section_one_services[]'))
                    <p class="help-block">
                        {{ $errors->first('section_one_services[]') }}
                    </p>
            @endif

            <button class="btn btn-info add_more_services_section_one" type="button">Add More Service</button>

            <hr/>
            <h2>Section 2</h2>
            <hr/>

            <div class="form-group {{ $errors->has('section_two_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_two_title" name="section_two_title" class="form-control validate[required]" value="{{ old('section_two_title', isset($data->section_two_title) ? $data->section_two_title : '') }}">
                @if($errors->has('section_two_title'))
                    <p class="help-block">
                        {{ $errors->first('section_two_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_two_description') ? 'has-error' : '' }}">
                <label for="section_two_description">Description *</label>
                <textarea id="section_two_description" name="section_two_description" class="form-control validate[required] ">{{ old('section_two_description', isset($data->section_two_description) ? $data->section_two_description : '') }}</textarea>
                @if($errors->has('section_two_description'))
                    <p class="help-block">
                        {{ $errors->first('section_two_description') }}
                    </p>
                @endif
                
            </div>

            <hr/>
            <h2>Section 3</h2>
            <hr/>
            <label>Serivec Agreement </label>
            <div class="form-group {{ $errors->has('section_three_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_three_title" name="section_three_title" class="form-control validate[required]" value="{{ old('section_three_title', isset($data->section_three_title) ? $data->section_three_title : '') }}">
                @if($errors->has('section_three_title'))
                    <p class="help-block">
                        {{ $errors->first('section_three_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_three_detail') ? 'has-error' : '' }}">
                <label for="name">Detail*</label>
                <input type="text" id="section_three_detail" name="section_three_detail" class="form-control validate[required]" value="{{ old('section_three_detail', isset($data->section_three_detail) ? $data->section_three_detail : '') }}">
                @if($errors->has('section_three_detail'))
                    <p class="help-block">
                        {{ $errors->first('section_three_detail') }}
                    </p>
                @endif
                
            </div>

            @php
            $points = isset($data->section_three_points) && !empty($data->section_three_points) ? json_decode($data->section_three_points) : array() ;
            @endphp
            @if(is_array($points) && sizeof($points) == 0 )
            <div class="form-group point_remove_div  {{ $errors->has('section_three_points') ? 'has-error' : '' }}">
                <label for="name">Point*</label>
                <input type="text" id="section_two_title" name="section_three_points[]" class="form-control validate[required]" value="">
            </div>
            @endif

            <div id="add_more_points_section_two_result">
                @foreach($points as $p_key => $value)
                <div class="form-group point_remove_div remove_div{{$p_key+1}}" >
                    <label for="name"> Point*</label>
                    <button type="button" class="btn btn-danger  add_more_remove float-right" counter="{{$p_key+1}}"> Remove</button>
                    <input type="text"  name="section_three_points[]" class="form-control validate[required] " value="{{ $value ?? '' }}">
                </div>
                @endforeach
            </div>
             @if($errors->has('section_three_points'))
                    <p class="help-block">
                        {{ $errors->first('section_three_points') }}
                    </p>
            @endif

            <button class="btn btn-info add_more_points_section_two" type="button">Add More Points</button>

            <br/>
            <br/>
            <label>Serivece Provider </label>
            <div class="form-group {{ $errors->has('section_three_provide_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_three_provide_title" name="section_three_provide_title" class="form-control validate[required]" value="{{ old('section_three_provide_title', isset($data->section_three_provide_title) ? $data->section_two_title : '') }}">
                @if($errors->has('section_three_provide_title'))
                    <p class="help-block">
                        {{ $errors->first('section_three_provide_title') }}
                    </p>
                @endif
                
            </div>
            @php
            $features = isset($data->section_three_features) && !empty($data->section_three_features) ? json_decode($data->section_three_features) : array() ;
            @endphp
            @if(is_array($features) && sizeof($features) == 0 )
            <div class="form-group feature_remove_div  {{ $errors->has('section_three_features') ? 'has-error' : '' }}">
                <label for="name">Feature*</label>
                <input type="text" id="section_three_features" name="section_three_features[]" class="form-control validate[required]" value="">
            </div>
            @endif

            <div id="section_two_feature_result">
                @foreach($features as $p_key => $value)
                <div class="form-group feature_remove_div remove_div{{$p_key+1}}" >
                    <label for="name"> Feature*</label>
                    <button type="button" class="btn btn-danger   add_more_remove float-right" counter="{{$p_key+1}}"> Remove</button>
                    <input type="text"  name="section_three_features[]" class="form-control validate[required]" value="{{ $value ?? '' }}">
                </div>
                @endforeach
            </div>
            @if($errors->has('section_three_features[]'))
                    <p class="help-block">
                        {{ $errors->first('section_three_features[]') }}
                    </p>
            @endif

            <button class="btn btn-info section_two_feature" type="button">Add More Features</button>
            <br />
            <br />
            

            <div class="form-group {{ $errors->has('section_three_image') ? 'has-error' : '' }}">
                            <label for="name"> Image *</label>
                             @if(isset($data->section_three_image) && file_exists($data->section_three_image))
                            <img src="{{ URL::to($data->section_three_image) }}" height="100" width="100" style="background: black;">
                            <input type="hidden" name="old_section_three_image" value="{{ $data->section_three_image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 658X526 pixels.</p>
                            <input class="{{isset($data->section_three_image) && file_exists($data->section_three_image) ?'' : 'validate[required]'}}"  type="file" name="section_three_image" accept="image/*">
                            @if($errors->has('section_three_image'))
                            <p class="help-block">
                                {{ $errors->first('section_three_image') }}
                            </p>
                            @endif
                </div>

            
            
            <div>
                <input type="hidden" name="id" value="{{ isset($transport_data->id) ? $transport_data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection