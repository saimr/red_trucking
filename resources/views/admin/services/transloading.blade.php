@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Transloading</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/services/updatetransloading') }}" class="validate_form" method="POST" enctype="multipart/form-data">
            @csrf
            @php
            $data = isset($transloading_data->meta_data) && !empty($transloading_data->meta_data) ? json_decode($transloading_data->meta_data) : array() ;
            @endphp 
            <hr/>
            <h2>Section 1</h2>
            <hr/>

            <div class="form-group {{ $errors->has('section_one_title') ? 'has-error' : '' }}">
                <label for="name"> Title*</label>
                <input type="text" id="section_one_title" name="section_one_title" class="form-control validate[required]" value="{{ old('section_one_title', isset($data->section_one_title) ? $data->section_one_title : '') }}">
                @if($errors->has('section_one_title'))
                    <p class="help-block">
                        {{ $errors->first('section_one_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_description') ? 'has-error' : '' }}">
                <label for="section_one_description">Description *</label>
                <textarea id="section_one_description" name="section_one_description" class="form-control validate[required] ">{{ old('section_one_description', isset($data->section_one_description) ? $data->section_one_description : '') }}</textarea>
                @if($errors->has('section_one_description'))
                    <p class="help-block">
                        {{ $errors->first('section_one_description') }}
                    </p>
                @endif
                
            </div>

            


            <hr/>
            <h2>Section 2</h2>
            <hr/>

            <div class="form-group {{ $errors->has('section_two_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_two_title" name="section_two_title" class="form-control validate[required]" value="{{ old('section_two_title', isset($data->section_two_title) ? $data->section_two_title : '') }}">
                @if($errors->has('section_two_title'))
                    <p class="help-block">
                        {{ $errors->first('section_two_title') }}
                    </p>
                @endif
                
            </div>

             @php
            $services = isset($data->section_two_services) && !empty($data->section_two_services) ? json_decode($data->section_two_services) : array() ;
            @endphp
            
            @if(is_array($services) && sizeof($services) == 0 )
            <div class="form-group service_remove_div">
                <label for="name"> Service*</label>
                <input type="text"  name="section_two_services[]" class=" form-control validate[required]" value="">
            </div>
            @endif

            <div id="add_more_services_section_two_result">
                 @foreach($services as $key => $value)
                <div class="form-group service_remove_div remove_div{{$key+1}}" >
                    <label for="name"> Service*</label>
                    <button type="button" class="btn btn-danger   add_more_remove float-right" counter="{{$key+1}}"> Remove</button>
                    <input type="text"  name="section_two_services[]" class=" validate[required] form-control" value="{{ $value ?? '' }}">
                </div>
                @endforeach
            </div>
            @if($errors->has('section_two_services[]'))
                    <p class="help-block">
                        {{ $errors->first('section_two_services[]') }}
                    </p>
            @endif

            <button class="btn btn-info add_more_services_section_two" type="button">Add More Service</button>



            

            <hr/>
            <h2>Section 3</h2>
            <hr/>

            <div class="form-group {{ $errors->has('section_third_feature_title') ? 'has-error' : '' }}">
                <label for="name">Feature Title*</label>
                <input type="text" id="section_third_feature_title" name="section_third_feature_title" class="form-control validate[required]" value="{{ old('section_third_feature_title', isset($data->section_third_feature_title) ? $data->section_third_feature_title : '') }}">
                @if($errors->has('section_third_feature_title'))
                    <p class="help-block">
                        {{ $errors->first('section_third_feature_title') }}
                    </p>
                @endif
                
            </div>


            @php
            $features = isset($data->section_third_features) && !empty($data->section_third_features) ? json_decode($data->section_third_features) : array() ;
            @endphp

            
            @if(is_array($features) && sizeof($features) == 0 )
            <div class="form-group feature_remove_div">
                <label for="name"> Feature*</label>
                <input type="text"  name="section_third_features[]" class=" form-control validate[required]" value="">
            </div>
            @endif

            <div id="add_more_section_third_features_result">
                 @foreach($features as $key => $value)
                <div class="form-group feature_remove_div remove_div{{$key+1}}" >
                    <label for="name"> Feature*</label>
                    <button type="button" class="btn btn-danger   add_more_remove float-right" counter="{{$key+1}}"> Remove</button>
                    <input type="text"  name="section_third_features[]" class=" validate[required] form-control" value="{{ $value ?? '' }}">
                </div>
                @endforeach
            </div>
            @if($errors->has('section_third_features[]'))
                    <p class="help-block">
                        {{ $errors->first('section_third_features[]') }}
                    </p>
            @endif

            <button class="btn btn-info add_more_section_third_features" type="button">Add More Features</button>


            
            <div class="form-group {{ $errors->has('section_third_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_third_title" name="section_third_title" class="form-control validate[required]" value="{{ old('section_third_title', isset($data->section_third_title) ? $data->section_third_title : '') }}">
                @if($errors->has('section_third_title'))
                    <p class="help-block">
                        {{ $errors->first('section_third_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_third_description') ? 'has-error' : '' }}">
                <label for="section_third_description">Description *</label>
                <textarea id="section_third_description" name="section_third_description" class="form-control validate[required] ">{{ old('section_third_description', isset($data->section_third_description) ? $data->section_third_description : '') }}</textarea>
                @if($errors->has('section_third_description'))
                    <p class="help-block">
                        {{ $errors->first('section_third_description') }}
                    </p>
                @endif
                
            </div>

           

            <div class="form-group {{ $errors->has('section_third_image') ? 'has-error' : '' }}">
                            <label for="name"> Image *</label>
                             @if(isset($data->section_third_image) && file_exists($data->section_third_image))
                            <img src="{{ URL::to($data->section_third_image) }}" height="100" width="100" style="background: black;">
                            <input type="hidden" name="old_section_third_image" value="{{ $data->section_third_image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 698X526 pixels.</p>
                            <input class="{{isset($data->section_third_image) && file_exists($data->section_third_image) ?'' : 'validate[required]'}}"  type="file" name="section_third_image" accept="image/*">
                            @if($errors->has('section_third_image'))
                            <p class="help-block">
                                {{ $errors->first('section_third_image') }}
                            </p>
                            @endif
                </div>

            
            
            <div>
                <input type="hidden" name="id" value="{{ isset($transloading_data->id) ? $transloading_data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection