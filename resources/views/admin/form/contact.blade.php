@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Contact Forms</b></h4>
    </div>
        <div class="card-body table-responsive p-0">
           @if(!empty($form_data) && sizeof($form_data))
           <div class="float-right">
              <a href="{{ URL::to('admin/form/contact/export') }}" class="btn btn-info">Export to CSV</a>          
            </div>

                <table class="table table-responsive table-bordered">
                  <thead>
                    <tr>
                       <th>Query_Date</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone_No</th>
                      <th>Best_Time</th>
                      <th>Message</th>
                      <th>Transport_Type</th>
                      <th>Company_Name</th>
                      <th>Commodity</th>
                      <th>No._of_Loads</th>
                      <th>Cargo_Weight</th>
                      <th>Pick Up Address</th>
                      <th>City</th>
                      <th>Sate</th>
                      <th>Zip_Code</th>
                      <th>Intermodal_Equipment</th>
                      <th>Termination Address</th>
                      <th>City</th>
                      <th>State</th>
                      <th>Zip_Code</th>
                      <th>Intermodal_Equipment</th>
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($form_data as $key => $value)
                    <tr>
                      <td>{{ $value->created_at ? date('d-M-Y',strtotime($value->created_at)) : '' }}</td>
                      <td>{{ $value->first_name ?? '' }} {{ $value->last_name ?? '' }}</td>
                      <td>{{ $value->email ?? '' }}</td>
                      <td>{{ $value->phone ?? '' }}</td>
                      
                      <td>{{ !empty($value->best_time) ?  date('h:i:A',$value->best_time) : '' }}</td>
                      <td>{{ $value->message ?? '' }}</td>
                      <td>
                        @if($value->transport_type == 1)
                        Warehouse/Transloading
                        @else
                        Sales Quotes
                        @endif

                         </td>
                      <td>{{ $value->company_name ?? '' }} </td>
                      <td>{{ $value->commodity ?? '' }} </td>
                      <td>{{ $value->number_of_loads ?? '' }} </td>
                      <td>{{ $value->cargo_weight ?? '' }} </td>
                      <td>{{ $value->pickup_address ?? '' }} </td>
                      <td>{{ $value->pickup_city ?? '' }} </td>
                      <td>{{ $value->pickup_state ?? '' }} </td>
                      <td>{{ $value->pickup_zip_code ?? '' }} </td>
                      <td>{{ $value->pickup_intermodal_equipment ?? '' }} </td>
                      <td>{{ $value->destination_address ?? '' }} </td>
                      <td>{{ $value->destination_city ?? '' }} </td>
                      <td>{{ $value->destination_state ?? '' }} </td>
                      <td>{{ $value->destination_zip_code ?? '' }} </td>
                      <td>{{ $value->destination_intermodal_equipment ?? '' }} </td>
                      
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
                @else
                    <div align="center">
                      <h2>No Record found !</h2>                      
                    </div>
                    @endif
        </div>

        <div class="card-footer clearfix">
                 {!! $form_data->links() !!}
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection