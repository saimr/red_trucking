@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>{{ config('constants.website_name') }} Partner</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/partner/update') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($data->title) ? $data->title : '') }}">
                @if($errors->has('title'))
                    <p class="help-block">
                        {{ $errors->first('title') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('sort_description') ? 'has-error' : '' }}">
                <label for="sort_description">Sort Description</label>
                <textarea rows="5"  id="sort_description" name="sort_description" class="form-control ">{{ old('sort_description', isset($data->sort_description) ? $data->sort_description : '') }}</textarea>
                @if($errors->has('sort_description'))
                    <p class="help-block">
                        {{ $errors->first('sort_description') }}
                    </p>
                @endif
                
            </div>

             <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Description *</label>
                <textarea rows="10"  id="description" name="description" class="form-control tinymce_editor">{{ old('description', isset($data) ? $data->description : '') }}</textarea>
                @if($errors->has('description'))
                    <p class="help-block">
                        {{ $errors->first('description') }}
                    </p>
                @endif
                
            </div>

            <!-- <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Upload Image</label> 

                @if(isset($image_data->name) && file_exists($image_data->name))
                
                <img src="{{ URL::to($image_data->name) }}" height="100" width="100">
                @endif

                 @if(isset($image_data->id))
                   <input type="hidden" name="gallery_id" value="{{ $image_data->id }}">
                   <input type="hidden" name="gallery_image_name" value="{{ $image_data->name }}">
                @endif

                <br/>
                <input type="file" name="images" accept="image/*">
                @if($errors->has('images'))
                    <p class="help-block">
                        {{ $errors->first('images') }}
                    </p>
                @endif
                
            </div> -->
            
            <div>
                <input type="hidden" name="id" value="{{ isset($data->id) ? $data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection