<!DOCTYPE html>
<html>
<head>
    <title>Contact us</title>
    <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body> 

	<table>
  <tr >
    <th colspan="2" style="text-align: center;">RED Trucking Contact Query </th>
  </tr>
  <tr>
    <th>First Name :</th>
    <td>{{ isset($data->first_name) ? $data->first_name : '' }}</td>
  </tr>
  <tr>
    <th>Last Name :</th>
    <td>{{ isset($data->last_name) ? $data->last_name : '' }}</td>
  </tr>
  <tr>
    <th>Phone :</th>
    <td>{{ isset($data->phone) ? $data->phone : '' }}</td>
  </tr>
  <tr>
    <th>Email :</th>
    <td>{{ isset($data->email) ? $data->email : '' }}</td>
  </tr>
  <tr>
    <th>Transport Type :</th>
    <td>
    	@if($data->transport_type == 1)
      Warehouse / Transloading
      @else
      Sales Quotes
      @endif
    </td>
  </tr>
  <tr>
    <th>Best Time :</th>
    <td>{{ isset($data->best_time) ? date('h:i:A',$data->best_time) : '' }}</td>
  </tr>
   <tr>
    <th>Message :</th>
    <td>{{ isset($data->message) ? $data->message : '' }}</td>
  </tr>
  
</table>
    
</body>
</html>