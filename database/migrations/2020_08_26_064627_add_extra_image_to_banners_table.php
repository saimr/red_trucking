<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraImageToBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banners', function (Blueprint $table) {
            //
            $table->string('second_image')->nullable();
            $table->string('third_image')->nullable();
            $table->string('log_title')->nullable();
            $table->string('logo_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banners', function (Blueprint $table) {
            //
            $table->dropColumn('second_image');
            $table->dropColumn('third_image');
            $table->dropColumn('log_title');
            $table->dropColumn('logo_image');
        });
    }
}
