<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsCdlToFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forms', function (Blueprint $table) {
            $table->tinyInteger('is_cdl')->length(1)->nullable();
            $table->tinyInteger('is_twice')->length(1)->nullable();
            $table->tinyInteger('is_driving_experience')->length(1)->comment('1->2 or more years')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forms', function (Blueprint $table) {
            $table->dropColumn('is_cdl');
            $table->dropColumn('is_twice');
            $table->dropColumn('is_driving_experience');
        });
    }
}
