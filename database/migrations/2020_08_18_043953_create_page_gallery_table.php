<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_gallery', function (Blueprint $table) {
            $table->id();
            $table->integer('page_id');
            $table->tinyInteger('type')->nullable()->comment('1->Video, 2->Image');
            $table->string('name');
            $table->tinyInteger('status')->length(1)->default(1);
            $table->tinyInteger('is_deleted')->length(1)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_gallery');
    }
}
