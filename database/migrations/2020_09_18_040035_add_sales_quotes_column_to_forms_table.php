<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSalesQuotesColumnToFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forms', function (Blueprint $table) {
            //
             $table->string('company_name')->nullable();
             $table->string('commodity')->nullable();
             $table->string('number_of_loads',10)->nullable();
             $table->string('cargo_weight',50)->nullable();
             $table->string('pickup_address')->nullable();
             $table->string('pickup_city')->nullable();
             $table->string('pickup_state')->nullable();
             $table->string('pickup_zip_code',10)->nullable();
             $table->string('pickup_intermodal_equipment')->nullable();
             $table->string('destination_address')->nullable();
             $table->string('destination_city')->nullable();
             $table->string('destination_state')->nullable();
             $table->string('destination_zip_code')->nullable();
             $table->string('destination_intermodal_equipment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forms', function (Blueprint $table) {
            //
            $table->dropColumn('company_name');
            $table->dropColumn('commodity');
            $table->dropColumn('number_of_loads');
            $table->dropColumn('cargo_weight');
            $table->dropColumn('pickup_address');
            $table->dropColumn('pickup_address');
            $table->dropColumn('pickup_city');
            $table->dropColumn('pickup_state');
            $table->dropColumn('pickup_zip_code');
            $table->dropColumn('pickup_intermodal_equipment');
            $table->dropColumn('destination_address');
            $table->dropColumn('destination_city');
            $table->dropColumn('destination_state');
            $table->dropColumn('destination_zip_code');
            $table->dropColumn('destination_intermodal_equipment');
        });
    }
}
