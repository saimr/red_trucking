<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetaDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_data', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('meta_title')->nullable();
            $table->tinyInteger('meta_type')->nullable()->comment('1->About Us Home Page Content,2-> TRUST Home Page Content,3-> SECURITY Home Page Content,4-> SUSTAINABILITY Home Page Content,5-> SERVICE AREAS Home Page Content, 6->Red Service Home Page Content, 7-> Partner Home Page Content, 8-> Resource Home Page Content');
            $table->longText('meta_data')->nullable();
            $table->tinyInteger('status')->length(1)->default(1);
            $table->tinyInteger('is_deleted')->length(1)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_data');
    }
}
