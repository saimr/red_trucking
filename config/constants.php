<?php
   
return [
    'app_url'=>  env('APP_URL'),
    'no_user_image' => 'images/users-image.png',
    'not_found_image' => 'images/not-found.png',
    'website_logo' => 'images/red-favicon.png',
    'website_favicon_image' => 'images/red-favicon.png',
    'website_name' => 'RED Trucking',
    'transport_type'=> [
                        'warehouse_transloading,'=>1,
                        'sales_quotes'=>2,
                       ],
    'form_type'=> [
                        'contact'=>1,
                        'career'=>2,
                       ],                   
    'about_us_page' => [
    					'about_us'=> 1,
    					'safety'=> 2,
    					'sustainability'=> 3,
    				   ],
    'services_page' => [
                        'transport'=> 4,
                        'warehousing'=> 5,
                        'logistics'=> 6,
                        'transloading'=> 7,
                       ],                   
    'page_gallery_type' => [
    					'video'=> 1,
    					'image'=> 2,
    				   ],
    'driver_page'   => 8 ,                         
    'partner_page'   => 9 ,   
    'activity_item_type'  => [
                            'login'=>1,
                            'logout'=>2,
                            'banner'=>3,
                           ]    , 
    'meta_type'  => [
                            'about_us_home_content'=>1,
                            'trust_home_content'=>2,
                            'security_home_content'=>3,
                            'sustainability_home_content'=>4,
                            'service_areas_home_content'=>5,
                            'red_service_home_content'=>6,
                            'partner_home_content'=>7,
                            'resource_home_content'=>8,
                            'about_us_owner_data'=>9,
                            'about_us_red_family_content'=>10,
                            'contact_us_page_details'=>11,
                            'safety_about_us_content'=>12,
                            'sustainability_about_us_content'=>13,
                            'safety_page'=>14,
                            'sustainability_page'=>15,
                            'red_driver_page'=>16,
                            'service_transport_page'=>17,
                            'service_warehousing_page'=>18,
                            'service_logistics_page'=>19,
                            'service_transloading_page'=>20,
                            'social_media_link'=>21,
                           ]    ,   
    'banner_type'=>[
                    'home_banner'=>1,
                    'red_driver_banner'=>2,
                   ]                      				   
]
  
?>